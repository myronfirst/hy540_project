#ifndef _SET_INCREASE_REF_COUNTER_TREE_VISITOR_H_
#define _SET_INCREASE_REF_COUNTER_TREE_VISITOR_H_

#include "TreeVisitor.h"
#include "TreeTags.h"


class IncreaseRefCounterVisitor final : public TreeVisitor {

public:

virtual TreeVisitor *Clone(void) const override { return new IncreaseRefCounterVisitor(); };

#define IMPL_VISIT(type)                                        \
    void Visit##type(const Object &node) override {             \
        Object *n = const_cast<Object *>(&node);                \
        n->IncreaseRefCounter();                                \
    }

    IMPL_VISIT(Program)
    IMPL_VISIT(Statements)
    IMPL_VISIT(Statement)
    IMPL_VISIT(Expression)
    IMPL_VISIT(Assign)
    IMPL_VISIT(Plus)
    IMPL_VISIT(Minus)
    IMPL_VISIT(Mul)
    IMPL_VISIT(Div)
    IMPL_VISIT(Modulo)
    IMPL_VISIT(Greater)
    IMPL_VISIT(Less)
    IMPL_VISIT(GreaterEqual)
    IMPL_VISIT(LessEqual)
    IMPL_VISIT(Equal)
    IMPL_VISIT(NotEqual)
    IMPL_VISIT(And)
    IMPL_VISIT(Or)
    IMPL_VISIT(Term)
    IMPL_VISIT(UnaryMinus)
    IMPL_VISIT(Not)
    IMPL_VISIT(PlusPlusBefore)
    IMPL_VISIT(PlusPlusAfter)
    IMPL_VISIT(MinusMinusBefore)
    IMPL_VISIT(MinusMinusAfter)
    IMPL_VISIT(Primary)
    IMPL_VISIT(MetaParse)
    IMPL_VISIT(MetaUnparse)
    IMPL_VISIT(MetaInline)
    IMPL_VISIT(MetaEscape)
    IMPL_VISIT(MetaQuasiQuotes)
    IMPL_VISIT(RuntimeEmpty)
    IMPL_VISIT(LValue)
    IMPL_VISIT(Id)
    IMPL_VISIT(Local)
    IMPL_VISIT(DoubleColon)
    IMPL_VISIT(Dollar)
    IMPL_VISIT(DollarEnv)
    IMPL_VISIT(DollarLambda)
    IMPL_VISIT(Member)
    IMPL_VISIT(Dot)
    IMPL_VISIT(Bracket)
    IMPL_VISIT(Call)
    IMPL_VISIT(ArgumentList)
    IMPL_VISIT(NamedArgument)
    IMPL_VISIT(ExpressionList)
    IMPL_VISIT(ObjectDef)
    IMPL_VISIT(Indexed)
    IMPL_VISIT(IndexedElem)
    IMPL_VISIT(Block)
    IMPL_VISIT(FunctionDef)
    IMPL_VISIT(Const)
    IMPL_VISIT(Number)
    IMPL_VISIT(String)
    IMPL_VISIT(Nil)
    IMPL_VISIT(True)
    IMPL_VISIT(False)
    IMPL_VISIT(IdList)
    IMPL_VISIT(Formal)
    IMPL_VISIT(If)
    IMPL_VISIT(While)
    IMPL_VISIT(For)
    IMPL_VISIT(Return)
    IMPL_VISIT(Break)
    IMPL_VISIT(Continue)
#undef IMPL_VISIT

};

#endif
