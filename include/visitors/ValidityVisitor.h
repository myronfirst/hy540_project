#ifndef _VALIDITY_VISITOR_H_
#define _VALIDITY_VISITOR_H_

#include "TreeVisitor.h"

#include <string>

class ValidityVisitor final : public TreeVisitor {
public:
    ValidityVisitor(){};
    virtual ~ValidityVisitor(){};
    TreeVisitor *Clone(void) const override { return new ValidityVisitor(); };

    void VisitDollarLambda(const Object &node) override;
    void VisitReturn(const Object &node) override;
    void VisitBreak(const Object &node) override;
    void VisitContinue(const Object &node) override;
    void VisitMetaEscape(const Object &node) override;
};

#endif
