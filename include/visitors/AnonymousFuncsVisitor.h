#ifndef _ANONYMOUS_FUNCS_VISITOR_H_
#define _ANONYMOUS_FUNCS_VISITOR_H_

#include "TreeVisitor.h"

#include <string>

class AnonymousFuncsVisitor final : public TreeVisitor {
private:
    unsigned nameOffset = 0;

public:
    AnonymousFuncsVisitor(void) =default;

    TreeVisitor *Clone(void) const override;

    void SetNameOffset(unsigned num);

    unsigned GetNameOffset(void);

    void VisitFunctionDef(const Object &node) override;

    virtual ~AnonymousFuncsVisitor() =default;
};

#endif
