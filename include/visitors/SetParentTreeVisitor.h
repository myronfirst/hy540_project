#ifndef _SET_PARENT_TREE_VISITOR_H_
#define _SET_PARENT_TREE_VISITOR_H_

#include "TreeVisitor.h"

#include "TreeTags.h"
#include "ValueStack.h"

class SetParentTreeVisitor final : public TreeVisitor {
private:
    ValueStack valueStack;
    bool isRemoval;
    void SetParent(const Object &parent, unsigned children);

public:
    SetParentTreeVisitor(bool _isRemoval = false) : isRemoval(_isRemoval) {}
    virtual ~SetParentTreeVisitor() {}
    virtual TreeVisitor *Clone(void) const override { return new SetParentTreeVisitor(isRemoval); };
    void SetIsRemoval(bool _isRemoval) { isRemoval = _isRemoval; }

#define IMPL_VISIT(type, children) \
    void Visit##type(const Object &node) override { SetParent(node, children); }

    IMPL_VISIT(Program, 1)
    IMPL_VISIT(Statements, node.GetNumericSize())
    IMPL_VISIT(Statement, (node.ElementExists(AST_TAG_CHILD) ? 1 : 0))
    IMPL_VISIT(Expression, 1)
    IMPL_VISIT(Assign, 2)
    IMPL_VISIT(Plus, 2)
    IMPL_VISIT(Minus, 2)
    IMPL_VISIT(Mul, 2)
    IMPL_VISIT(Div, 2)
    IMPL_VISIT(Modulo, 2)
    IMPL_VISIT(Greater, 2)
    IMPL_VISIT(Less, 2)
    IMPL_VISIT(GreaterEqual, 2)
    IMPL_VISIT(LessEqual, 2)
    IMPL_VISIT(Equal, 2)
    IMPL_VISIT(NotEqual, 2)
    IMPL_VISIT(And, 2)
    IMPL_VISIT(Or, 2)
    IMPL_VISIT(Term, 1)
    IMPL_VISIT(UnaryMinus, 1)
    IMPL_VISIT(Not, 1)
    IMPL_VISIT(PlusPlusBefore, 1)
    IMPL_VISIT(PlusPlusAfter, 1)
    IMPL_VISIT(MinusMinusBefore, 1)
    IMPL_VISIT(MinusMinusAfter, 1)
    IMPL_VISIT(Primary, 1)
    IMPL_VISIT(MetaParse, 1)
    IMPL_VISIT(MetaUnparse, 1)
    IMPL_VISIT(MetaInline, 1)
    IMPL_VISIT(MetaEscape, 1)
    IMPL_VISIT(MetaQuasiQuotes, 1)
    IMPL_VISIT(RuntimeEmpty, 0)
    IMPL_VISIT(LValue, 1)
    IMPL_VISIT(Id, 0)
    IMPL_VISIT(Local, 0)
    IMPL_VISIT(DoubleColon, 0)
    IMPL_VISIT(Dollar, 0)
    IMPL_VISIT(DollarEnv, 0)
    IMPL_VISIT(DollarLambda, 0)
    IMPL_VISIT(Member, 1)
    IMPL_VISIT(Dot, 2)
    IMPL_VISIT(Bracket, 2)
    IMPL_VISIT(Call, 2)
    IMPL_VISIT(ArgumentList, node.GetNumericSize())
    IMPL_VISIT(NamedArgument, node.GetNumericSize())
    IMPL_VISIT(ExpressionList, node.GetNumericSize())
    IMPL_VISIT(ObjectDef, 1)
    IMPL_VISIT(Indexed, node.GetNumericSize())
    IMPL_VISIT(IndexedElem, 2)
    IMPL_VISIT(Block, 1)
    IMPL_VISIT(FunctionDef, 3)
    IMPL_VISIT(Const, 1)
    IMPL_VISIT(Number, 0)
    IMPL_VISIT(String, 0)
    IMPL_VISIT(Nil, 0)
    IMPL_VISIT(True, 0)
    IMPL_VISIT(False, 0)
    IMPL_VISIT(IdList, node.GetNumericSize())
    IMPL_VISIT(Formal, 0)
    IMPL_VISIT(If, (node.ElementExists(AST_TAG_ELSE_STMT) ? 3 : 2))
    IMPL_VISIT(While, 2)
    IMPL_VISIT(For, 4)
    IMPL_VISIT(Return, (node.ElementExists(AST_TAG_CHILD) ? 1 : 0))
    IMPL_VISIT(Break, 0)
    IMPL_VISIT(Continue, 0)
#undef IMPL_VISIT
};

#endif
