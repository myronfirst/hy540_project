#ifndef _SANITY_VISITOR_H_
#define _SANITY_VISITOR_H_

#include "TreeVisitor.h"

#include <exception>
#include <fstream>
#include <sstream>
#include <stack>

class SanityException : public std::exception {
private:
    std::string msg;

public:
    SanityException(void)=default;
    SanityException(const std::string & msg);

    virtual const char* what() const throw() override;
};

class SanityVisitor final : public TreeVisitor {
public:
    virtual void VisitProgram(const Object &node) override;
    virtual void VisitStatements(const Object &node) override;
    virtual void VisitStatement(const Object &node) override;
    virtual void VisitExpression(const Object &node) override;
    virtual void VisitAssign(const Object &node) override;
    virtual void VisitPlus(const Object &node) override;
    virtual void VisitMinus(const Object &node) override;
    virtual void VisitMul(const Object &node) override;
    virtual void VisitDiv(const Object &node) override;
    virtual void VisitModulo(const Object &node) override;
    virtual void VisitGreater(const Object &node) override;
    virtual void VisitLess(const Object &node) override;
    virtual void VisitGreaterEqual(const Object &node) override;
    virtual void VisitLessEqual(const Object &node) override;
    virtual void VisitEqual(const Object &node) override;
    virtual void VisitNotEqual(const Object &node) override;
    virtual void VisitAnd(const Object &node) override;
    virtual void VisitOr(const Object &node) override;
    virtual void VisitTerm(const Object &node) override;
    virtual void VisitUnaryMinus(const Object &node) override;
    virtual void VisitNot(const Object &node) override;
    virtual void VisitPlusPlusBefore(const Object &node) override;
    virtual void VisitPlusPlusAfter(const Object &node) override;
    virtual void VisitMinusMinusBefore(const Object &node) override;
    virtual void VisitMinusMinusAfter(const Object &node) override;
    virtual void VisitPrimary(const Object &node) override;
    virtual void VisitMetaParse(const Object &node) override;
    virtual void VisitMetaUnparse(const Object &node) override;
    virtual void VisitMetaInline(const Object &node) override;
    virtual void VisitMetaEscape(const Object &node) override;
    virtual void VisitMetaQuasiQuotes(const Object &node) override;
    virtual void VisitRuntimeEmpty(const Object &node) override;
    virtual void VisitLValue(const Object &node) override;
    virtual void VisitId(const Object &node) override;
    virtual void VisitLocal(const Object &node) override;
    virtual void VisitDoubleColon(const Object &node) override;
    virtual void VisitDollar(const Object &node) override;
    virtual void VisitDollarEnv(const Object &node) override;
    virtual void VisitDollarLambda(const Object &node) override;
    virtual void VisitMember(const Object &node) override;
    virtual void VisitDot(const Object &node) override;
    virtual void VisitBracket(const Object &node) override;
    virtual void VisitCall(const Object &node) override;
    virtual void VisitArgumentList(const Object &node) override;
    virtual void VisitNamedArgument(const Object &node) override;
    virtual void VisitExpressionList(const Object &node) override;
    virtual void VisitObjectDef(const Object &node) override;
    virtual void VisitIndexed(const Object &node) override;
    virtual void VisitIndexedElem(const Object &node) override;
    virtual void VisitBlock(const Object &node) override;
    virtual void VisitFunctionDef(const Object &node) override;
    virtual void VisitConst(const Object &node) override;
    virtual void VisitNumber(const Object &node) override;
    virtual void VisitString(const Object &node) override;
    virtual void VisitNil(const Object &node) override;
    virtual void VisitTrue(const Object &node) override;
    virtual void VisitFalse(const Object &node) override;
    virtual void VisitIdList(const Object &node) override;
    virtual void VisitFormal(const Object &node) override;
    virtual void VisitIf(const Object &node) override;
    virtual void VisitWhile(const Object &node) override;
    virtual void VisitFor(const Object &node) override;
    virtual void VisitReturn(const Object &node) override;
    virtual void VisitBreak(const Object &node) override;
    virtual void VisitContinue(const Object &node) override;

    TreeVisitor *Clone(void) const override { return new SanityVisitor(); };

    SanityVisitor(void){};
    SanityVisitor(const std::string &filename);        //what are those?
    SanityVisitor(const SanityVisitor &) = default;    //what are those?

    virtual ~SanityVisitor(){};
};

#endif
