#ifndef _EVAL_META_ESCAPES_TREE_VISITOR_H_
#define _EVAL_META_ESCAPES_TREE_VISITOR_H_

#include "TreeVisitor.h"

#include <functional>

class EvalMetaEscapesTreeVisitor final : public TreeVisitor {
public:
    using EvalMetaEscape = std::function<void(const Object &)>;

private:
    EvalMetaEscape evalMetaEscape;

public:
    EvalMetaEscapesTreeVisitor() {}
    virtual ~EvalMetaEscapesTreeVisitor() {}
    TreeVisitor *Clone(void) const override { return new EvalMetaEscapesTreeVisitor(); };
    void SetEvalMetaEscape(EvalMetaEscape callback);

    void VisitMetaEscape(const Object &node) override { evalMetaEscape(node); }
};

#endif
