#ifndef _LIBRARY_FUNCTIONS_H_
#define _LIBRARY_FUNCTIONS_H_

#include "Object.h"
#include "Value.h"

#include <string>

namespace LibFunc {

    void Print(Object & env);

    void SimplePrint(Object & env);

    void Typeof(Object & env);

    void Eval(Object & env);

    void Include(Object & env);

    void Assert(Object & env);

    void Sqrt(Object & env);

    void Pow(Object & env);

    void Sin(Object & env);

    void Cos(Object & env);

    void Tan(Object & env);

    void Floor(Object & env);

    void Ceil(Object & env);

    void Abs(Object & env);

    void Round(Object & env);

    void Trunc(Object & env);

    void GetTime(Object & env);

    void ToNumber(Object & env);

    void ToString(Object & env);

    void Random(Object & env);

    void Input(Object & env);

    void Undef(Object & env);

    void FileOpen(Object & env);

    void FileClose(Object & env);

    void FileWrite(Object & env);

    void FileRead(Object & env);

    void FileRemove(Object & env);

    void ObjectKeys(Object & env);

    void ObjectSize(Object & env);

    void ObjectIncludes(Object & env);

    void ObjectPush(Object & env);

    void Sleep(Object & env);

    void Strlen(Object & env);

    void StrSearch(Object & env);

    void StrSubstr(Object & env);

    void StrReplace(Object & env);

    void StrTrim(Object & env);

    void Popen(Object & env);

    void Request(Object & env);
}

#endif