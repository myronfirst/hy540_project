#ifndef _PRE_ORDER_TREE_HOST_H_
#define _PRE_ORDER_TREE_HOST_H_

#include "TreeHost.h"

class PreOrderTreeHost final : public TreeHost {
public:
    PreOrderTreeHost(void) : TreeHost(){};
    PreOrderTreeHost(TreeVisitor *_visitor) : TreeHost(_visitor){};
    PreOrderTreeHost(const std::vector<TreeVisitor*>& _visitors) : TreeHost(_visitors){};

    void AcceptProgram(const Object &node) override;
    void AcceptStatements(const Object &node) override;
    void AcceptStatement(const Object &node) override;
    void AcceptExpression(const Object &node) override;
    void AcceptAssign(const Object &node) override;
    void AcceptPlus(const Object &node) override;
    void AcceptMinus(const Object &node) override;
    void AcceptMul(const Object &node) override;
    void AcceptDiv(const Object &node) override;
    void AcceptModulo(const Object &node) override;
    void AcceptGreater(const Object &node) override;
    void AcceptGreaterEqual(const Object &node) override;
    void AcceptLess(const Object &node) override;
    void AcceptLessEqual(const Object &node) override;
    void AcceptEqual(const Object &node) override;
    void AcceptNotEqual(const Object &node) override;
    void AcceptAnd(const Object &node) override;
    void AcceptOr(const Object &node) override;
    void AcceptTerm(const Object &node) override;
    void AcceptUnaryMinus(const Object &node) override;
    void AcceptNot(const Object &node) override;
    void AcceptPlusPlusBefore(const Object &node) override;
    void AcceptPlusPlusAfter(const Object &node) override;
    void AcceptMinusMinusBefore(const Object &node) override;
    void AcceptMinusMinusAfter(const Object &node) override;
    void AcceptPrimary(const Object &node) override;
    void AcceptMetaParse(const Object &node) override;
    void AcceptMetaUnparse(const Object &node) override;
    void AcceptMetaInline(const Object &node) override;
    void AcceptMetaEscape(const Object &node) override;
    void AcceptMetaQuasiQuotes(const Object &node) override;
    void AcceptRuntimeEmpty(const Object &node) override;
    void AcceptLValue(const Object &node) override;
    void AcceptId(const Object &node) override;
    void AcceptLocal(const Object &node) override;
    void AcceptDoubleColon(const Object &node) override;
    void AcceptDollar(const Object &node) override;
    void AcceptDollarLambda(const Object &node) override;
    void AcceptDollarEnv(const Object &node) override;
    void AcceptMember(const Object &node) override;
    void AcceptDot(const Object &node) override;
    void AcceptBracket(const Object &node) override;
    void AcceptCall(const Object &node) override;
    void AcceptArgumentList(const Object &node) override;
    void AcceptNamedArgument(const Object &node) override;
    void AcceptExpressionList(const Object &node) override;
    void AcceptObjectDef(const Object &node) override;
    void AcceptIndexed(const Object &node) override;
    void AcceptIndexedElem(const Object &node) override;
    void AcceptBlock(const Object &node) override;
    void AcceptFunctionDef(const Object &node) override;
    void AcceptConst(const Object &node) override;
    void AcceptNumber(const Object &node) override;
    void AcceptString(const Object &node) override;
    void AcceptNil(const Object &node) override;
    void AcceptTrue(const Object &node) override;
    void AcceptFalse(const Object &node) override;
    void AcceptIdList(const Object &node) override;
    void AcceptFormal(const Object &node) override;
    void AcceptIf(const Object &node) override;
    void AcceptWhile(const Object &node) override;
    void AcceptFor(const Object &node) override;
    void AcceptReturn(const Object &node) override;
    void AcceptBreak(const Object &node) override;
    void AcceptContinue(const Object &node) override;
};

#endif
