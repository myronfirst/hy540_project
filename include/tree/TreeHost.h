#ifndef _TREE_HOST_H_
#define _TREE_HOST_H_

#include "Object.h"
#include "TreeVisitor.h"

#include <functional>
#include <map>
#include <string>
#include <vector>

class TreeHost {
protected:
    using Acceptor = std::function<void(const Object &)>;
    using Acceptors = std::map<std::string, Acceptor>;

    Acceptors acceptors;
    std::vector<TreeVisitor*> visitors;

public:
    TreeHost(void);
    TreeHost(TreeVisitor *_visitor);
    TreeHost(const std::vector<TreeVisitor*>& _visitors);
    virtual ~TreeHost();
    void Accept(const Object &node);
    void InstallAcceptor(const std::string &tag, const Acceptor &f);
    void InstallAllAcceptors(void);
    void AddVisitor(TreeVisitor* visitor);
    std::vector<std::string> GetInstalledTags(void) const;

    virtual void AcceptProgram(const Object &node) = 0;
    virtual void AcceptStatements(const Object &node) = 0;
    virtual void AcceptStatement(const Object &node) = 0;
    virtual void AcceptExpression(const Object &node) = 0;
    virtual void AcceptAssign(const Object &node) = 0;
    virtual void AcceptPlus(const Object &node) = 0;
    virtual void AcceptMinus(const Object &node) = 0;
    virtual void AcceptMul(const Object &node) = 0;
    virtual void AcceptDiv(const Object &node) = 0;
    virtual void AcceptModulo(const Object &node) = 0;
    virtual void AcceptGreater(const Object &node) = 0;
    virtual void AcceptGreaterEqual(const Object &node) = 0;
    virtual void AcceptLess(const Object &node) = 0;
    virtual void AcceptLessEqual(const Object &node) = 0;
    virtual void AcceptEqual(const Object &node) = 0;
    virtual void AcceptNotEqual(const Object &node) = 0;
    virtual void AcceptAnd(const Object &node) = 0;
    virtual void AcceptOr(const Object &node) = 0;
    virtual void AcceptTerm(const Object &node) = 0;
    virtual void AcceptUnaryMinus(const Object &node) = 0;
    virtual void AcceptNot(const Object &node) = 0;
    virtual void AcceptPlusPlusBefore(const Object &node) = 0;
    virtual void AcceptPlusPlusAfter(const Object &node) = 0;
    virtual void AcceptMinusMinusBefore(const Object &node) = 0;
    virtual void AcceptMinusMinusAfter(const Object &node) = 0;
    virtual void AcceptPrimary(const Object &node) = 0;
    virtual void AcceptMetaParse(const Object &node) = 0;
    virtual void AcceptMetaUnparse(const Object &node) = 0;
    virtual void AcceptMetaInline(const Object &node) = 0;
    virtual void AcceptMetaEscape(const Object &node) = 0;
    virtual void AcceptMetaQuasiQuotes(const Object &node) = 0;
    virtual void AcceptRuntimeEmpty(const Object &node) = 0;
    virtual void AcceptLValue(const Object &node) = 0;
    virtual void AcceptId(const Object &node) = 0;
    virtual void AcceptLocal(const Object &node) = 0;
    virtual void AcceptDoubleColon(const Object &node) = 0;
    virtual void AcceptDollar(const Object &node) = 0;
    virtual void AcceptDollarLambda(const Object &node) = 0;
    virtual void AcceptDollarEnv(const Object &node) = 0;
    virtual void AcceptMember(const Object &node) = 0;
    virtual void AcceptDot(const Object &node) = 0;
    virtual void AcceptBracket(const Object &node) = 0;
    virtual void AcceptCall(const Object &node) = 0;
    virtual void AcceptArgumentList(const Object &node) = 0;
    virtual void AcceptNamedArgument(const Object &node) = 0;
    virtual void AcceptExpressionList(const Object &node) = 0;
    virtual void AcceptObjectDef(const Object &node) = 0;
    virtual void AcceptIndexed(const Object &node) = 0;
    virtual void AcceptIndexedElem(const Object &node) = 0;
    virtual void AcceptBlock(const Object &node) = 0;
    virtual void AcceptFunctionDef(const Object &node) = 0;
    virtual void AcceptConst(const Object &node) = 0;
    virtual void AcceptNumber(const Object &node) = 0;
    virtual void AcceptString(const Object &node) = 0;
    virtual void AcceptNil(const Object &node) = 0;
    virtual void AcceptTrue(const Object &node) = 0;
    virtual void AcceptFalse(const Object &node) = 0;
    virtual void AcceptIdList(const Object &node) = 0;
    virtual void AcceptFormal(const Object &node) = 0;
    virtual void AcceptIf(const Object &node) = 0;
    virtual void AcceptWhile(const Object &node) = 0;
    virtual void AcceptFor(const Object &node) = 0;
    virtual void AcceptReturn(const Object &node) = 0;
    virtual void AcceptBreak(const Object &node) = 0;
    virtual void AcceptContinue(const Object &node) = 0;
};

#endif
