#ifndef _HIDDEN_TAGS_H_
#define _HIDDEN_TAGS_H_

/****** Reserved Fields ******/

#define PARENT_RESERVED_FIELD "$parent"
#define UNPARSE_VALUE_RESERVED_FIELD "$unparse_value"
#define CLONED_TREE_RESERVED_FIELD "$cloned_tree"

#define PREVIOUS_RESERVED_FIELD "$previous"
#define OUTER_RESERVED_FIELD "$outer"
#define RETVAL_RESERVED_FIELD "$retval"
#define CLOSURE_RESERVED_FIELD "$closure"
#define ENV_RESERVED_FIELD "$env"
#define LAMBDA_RESERVER_FIELD "$lambda"
#define ENV_RESERVED_FIELD "$env"
#define LOCAL_RESERVED_FIELD "$local"
#define LINE_NUMBER_RESERVED_FIELD "$line_number"
#define FUNCTION_AST_RESERVED_FIELD "$ast"    //used?

#endif
