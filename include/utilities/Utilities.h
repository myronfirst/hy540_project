#ifndef _UTILITIES_H_
#define _UTILITIES_H_

#include <string>

#ifndef SET_LINE
#define SET_LINE(node) node->Set(LINE_NUMBER_RESERVED_FIELD, double(yylineno))
#endif
#ifndef GET_LINE
#define GET_LINE(node) static_cast<unsigned>((node)[LINE_NUMBER_RESERVED_FIELD]->ToNumber())
#endif

#define RED_COLOR "\033[31;1m"
#define YELLOW_COLOR "\033[33;1m"
#define NO_COLOR "\033[0m"

namespace Utilities {

    bool IsZero(double);

    bool IsInt(double);

    bool DoublesAreEqual(double a, double b);

    std::string UnparserFormatEscChars(const std::string &str);

    void SyntaxError(const std::string &msg, unsigned line = 0);

    void SyntaxWarning(const std::string &msg, unsigned line = 0);

    std::string TrimString(const std::string & str);

    void SetStackSize(unsigned size);

}    // namespace Utilities

#endif
