#include "AnonymousFuncsVisitor.h"
#include "CloneTreeVisitor.h"
#include "Interpreter.h"
#include "LibraryFunctions.h"
#include "PostOrderTreeHost.h"
#include "PreOrderTreeHost.h"
#include "SanityVisitor.h"
#include "SetParentTreeVisitor.h"
#include "TreeHost.h"
#include "TreeTags.h"
#include "Utilities.h"
#include "ValidityVisitor.h"
#include "VisualizeVisitor.h"
#include "IncreaseRefCountVisitor.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <set>
#include <sstream>

#define FUNCTION_AST_FIELD

/****** Start-up ******/

void Interpreter::InstallEvaluators(void) {
    /* Evaluators used for read-only access */
    INSTALL(AST_TAG_PROGRAM, EvalProgram);
    INSTALL(AST_TAG_STMTS, EvalStatements);
    INSTALL(AST_TAG_STMT, EvalStatement);
    INSTALL(AST_TAG_EXPR, EvalExpression);
    INSTALL(AST_TAG_ASSIGN, EvalAssign);
    INSTALL(AST_TAG_PLUS, EvalPlus);
    INSTALL(AST_TAG_MINUS, EvalMinus);
    INSTALL(AST_TAG_MUL, EvalMul);
    INSTALL(AST_TAG_DIV, EvalDiv);
    INSTALL(AST_TAG_MODULO, EvalModulo);
    INSTALL(AST_TAG_GREATER, EvalGreater);
    INSTALL(AST_TAG_LESS, EvalLess);
    INSTALL(AST_TAG_GEQUAL, EvalGreaterEqual);
    INSTALL(AST_TAG_LEQUAL, EvalLessEqual);
    INSTALL(AST_TAG_TERM, EvalTerm);
    INSTALL(AST_TAG_PRIMARY, EvalPrimary);
    INSTALL(AST_TAG_META_PARSE, EvalMetaParse);
    INSTALL(AST_TAG_META_UNPARSE, EvalMetaUnparse);
    INSTALL(AST_TAG_META_INLINE, EvalMetaInline);
    INSTALL(AST_TAG_META_QUASI_QUOTES, EvalMetaQuasiQuotes);
    INSTALL(AST_TAG_RUNTIME_EMPTY, EvalRuntimeEmpty);
    INSTALL(AST_TAG_LVALUE, EvalLValue);
    INSTALL(AST_TAG_CONST, EvalConst);
    INSTALL(AST_TAG_NUMBER, EvalNumber);
    INSTALL(AST_TAG_STRING, EvalString);
    INSTALL(AST_TAG_NIL, EvalNil);
    INSTALL(AST_TAG_TRUE, EvalTrue);
    INSTALL(AST_TAG_FALSE, EvalFalse);
    INSTALL(AST_TAG_ID, EvalId);
    INSTALL(AST_TAG_DOUBLECOLON_ID, EvalDoubleColon);
    INSTALL(AST_TAG_LOCAL_ID, EvalLocal);
    INSTALL(AST_TAG_FUNCTION_DEF, EvalFunctionDef);
    INSTALL(AST_TAG_EQUAL, EvalEqual);
    INSTALL(AST_TAG_NEQUAL, EvalNotEqual);
    INSTALL(AST_TAG_AND, EvalAnd);
    INSTALL(AST_TAG_OR, EvalOr);
    INSTALL(AST_TAG_UMINUS, EvalUnaryMinus);
    INSTALL(AST_TAG_NOT, EvalNot);
    INSTALL(AST_TAG_BPLUSPLUS, EvalPlusPlusBefore);
    INSTALL(AST_TAG_APLUSPLUS, EvalPlusPlusAfter);
    INSTALL(AST_TAG_BMINUSMINUS, EvalMinusMinusBefore);
    INSTALL(AST_TAG_AMINUSMINUS, EvalMinusMinusAfter);
    INSTALL(AST_TAG_ELIST, EvalExpressionList);
    INSTALL(AST_TAG_ARGLIST, EvalArgumentList);
    INSTALL(AST_TAG_NAMED, EvalNamedArgument);
    INSTALL(AST_TAG_CALL, EvalCall);
    INSTALL(AST_TAG_BLOCK, EvalBlock);
    INSTALL(AST_TAG_IF, EvalIf);
    INSTALL(AST_TAG_WHILE, EvalWhile);
    INSTALL(AST_TAG_FOR, EvalFor);
    INSTALL(AST_TAG_BREAK, EvalBreak);
    INSTALL(AST_TAG_CONTINUE, EvalContinue);
    INSTALL(AST_TAG_RETURN, EvalReturn);
    INSTALL(AST_TAG_INDEXED_ELEM, EvalIndexedElem);
    INSTALL(AST_TAG_INDEXED, EvalIndexed);
    INSTALL(AST_TAG_OBJECT_DEF, EvalObjectDef);
    INSTALL(AST_TAG_MEMBER, EvalMember);
    INSTALL(AST_TAG_DOT, EvalDot);
    INSTALL(AST_TAG_DOLLAR_ENV, EvalDollarEnv);
    INSTALL(AST_TAG_DOLLAR_LAMBDA, EvalDollarLambda);
    INSTALL(AST_TAG_BRACKET, EvalBracket);
    /* Evaluators used for write access */
    INSTALL_WRITE_FUNC(AST_TAG_LVALUE, EvalLvalueWrite);
    INSTALL_WRITE_FUNC(AST_TAG_MEMBER, EvalMemberWrite);
    INSTALL_WRITE_FUNC(AST_TAG_DOT, EvalDotWrite);
    INSTALL_WRITE_FUNC(AST_TAG_BRACKET, EvalBracketWrite);
    INSTALL_WRITE_FUNC(AST_TAG_ID, EvalIdWrite);
    INSTALL_WRITE_FUNC(AST_TAG_DOUBLECOLON_ID, EvalGlobalIdWrite);
    INSTALL_WRITE_FUNC(AST_TAG_LOCAL_ID, EvalLocalIdWrite);
    INSTALL_WRITE_FUNC(AST_TAG_FORMAL, EvalFormalWrite);
    INSTALL_WRITE_FUNC(AST_TAG_DOLLAR_ENV, EvalDollarEnvWrite);
    INSTALL_WRITE_FUNC(AST_TAG_DOLLAR_LAMBDA, EvalDollarLambdaWrite);
}

#define INSTALL_LIB_FUNC(x, y)                 \
    globalScope->Set(x, Value(LibFunc::y, x)); \
    libraryFuncs.push_front(x);

void Interpreter::InstallLibFuncs(void) {
    INSTALL_LIB_FUNC("print", Print);
    INSTALL_LIB_FUNC("sprint", SimplePrint);
    INSTALL_LIB_FUNC("typeof", Typeof);
    INSTALL_LIB_FUNC("object_keys", ObjectKeys);
    INSTALL_LIB_FUNC("object_size", ObjectSize);
    INSTALL_LIB_FUNC("sleep", Sleep);
    INSTALL_LIB_FUNC("eval", Eval);
    INSTALL_LIB_FUNC("include", Include);
    INSTALL_LIB_FUNC("assert", Assert);
    INSTALL_LIB_FUNC("sqrt", Sqrt);
    INSTALL_LIB_FUNC("pow", Pow);
    INSTALL_LIB_FUNC("sin", Sin);
    INSTALL_LIB_FUNC("cos", Cos);
    INSTALL_LIB_FUNC("tan", Tan);
    INSTALL_LIB_FUNC("floor", Floor);
    INSTALL_LIB_FUNC("ceil", Ceil);
    INSTALL_LIB_FUNC("abs", Abs);
    INSTALL_LIB_FUNC("round", Round);
    INSTALL_LIB_FUNC("trunc", Trunc);
    INSTALL_LIB_FUNC("get_time", GetTime);
    INSTALL_LIB_FUNC("input", Input);
    INSTALL_LIB_FUNC("file_open", FileOpen);
    INSTALL_LIB_FUNC("file_close", FileClose);
    INSTALL_LIB_FUNC("file_write", FileWrite);
    INSTALL_LIB_FUNC("file_read", FileRead);
    INSTALL_LIB_FUNC("to_number", ToNumber);
    INSTALL_LIB_FUNC("rand", Random);
    INSTALL_LIB_FUNC("undef", Undef);
    INSTALL_LIB_FUNC("strlen", Strlen);
    INSTALL_LIB_FUNC("string_search", StrSearch);
    INSTALL_LIB_FUNC("string_substr", StrSubstr);
    INSTALL_LIB_FUNC("string_replace", StrReplace);
    INSTALL_LIB_FUNC("string_trim", StrTrim);
    INSTALL_LIB_FUNC("popen", Popen);
    INSTALL_LIB_FUNC("file_remove", FileRemove);
    INSTALL_LIB_FUNC("to_string", ToString);
    INSTALL_LIB_FUNC("object_includes", ObjectIncludes);
    INSTALL_LIB_FUNC("object_push", ObjectPush);
    INSTALL_LIB_FUNC("request", Request);
}

/****** Public Members ******/

Interpreter::Interpreter(void) {
    globalScope = new Object();
    globalScope->IncreaseRefCounter();
    scopeStack.push_front(globalScope);

    currentScope = globalScope;
    currentScope->IncreaseRefCounter();
    inFunctionScope = false;

    InstallLibFuncs();
    InstallEvaluators();
}

const Value Interpreter::Execute(Object &program) {
    assert(program[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_PROGRAM);
    assert(program.ElementExists(AST_TAG_CHILD));

    debugAst = &program;

    return EvalProgram(program);
}

void Interpreter::RuntimeError(const std::string &msg, unsigned line) const {
    std::string lineMsg = (line != 0) ? (" at line " + std::to_string(line)) : (lineNumber != 0) ? (" at line " + std::to_string(lineNumber)) : "";

    std::cerr << RED_COLOR
              << "Runtime Error" << lineMsg << ": "
              << NO_COLOR << msg << std::endl;
    exit(EXIT_FAILURE);
}

void Interpreter::RuntimeWarning(const std::string &msg, unsigned line) const {
    std::string lineMsg = (line != 0) ? (" at line " + std::to_string(line)) : (lineNumber != 0) ? (" at line " + std::to_string(lineNumber)) : "";

    std::cerr << YELLOW_COLOR
              << "Runtime Warning" << lineMsg << ": "
              << NO_COLOR << msg << std::endl;
}

void Interpreter::Assert(const std::string &msg, unsigned line) const {
    std::string lineMsg = (line != 0) ? (" at line " + std::to_string(line)) : (lineNumber != 0) ? (" at line " + std::to_string(lineNumber)) : "";

    std::cerr << RED_COLOR
              << "Assertion Failed" << lineMsg << ": "
              << NO_COLOR << msg << std::endl;
    exit(EXIT_FAILURE);
}

Interpreter::~Interpreter() {
    /* Cleanup */
    CleanUpGlobalScope();
    dispatcher.Clear();
    libraryFuncs.clear();
    scopeStack.clear();
}

/****** Lvalue Write Access ******/

Symbol Interpreter::EvalLvalueWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_LVALUE);
    return EVAL_WRITE(AST_TAG_CHILD);
}

Symbol Interpreter::EvalMemberWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_MEMBER);
    return EVAL_WRITE(AST_TAG_CHILD);
}

Symbol Interpreter::EvalDotWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_DOT);

    auto lvalue = EVAL(AST_TAG_LVALUE);
    auto index = GetIdName(*node[AST_TAG_ID]->ToObject());

    CHANGE_LINE();

    if (IS_CLOSURE_CHANGE())
        return EvalClosureWrite(node);
    else
        return TableSetElem(lvalue, index);
}

Symbol Interpreter::EvalBracketWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_BRACKET);
    const Value lvalue = EVAL(AST_TAG_LVALUE);
    const Value index = EVAL(AST_TAG_EXPR);

    CHANGE_LINE();

    if (IS_CLOSURE_CHANGE())
        return EvalClosureWrite(node);
    else
        return TableSetElem(lvalue, index);
}

Symbol Interpreter::EvalIdWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_ID);

    std::string name = node[AST_TAG_ID]->ToString();
    Object *scope = FindScope(name);
    if (!scope) scope = currentScope;

    return Symbol(scope, name);
}

Symbol Interpreter::EvalGlobalIdWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_DOUBLECOLON_ID);
    CHANGE_LINE();

    std::string symbol = node[AST_TAG_ID]->ToString();
    if (!LookupGlobalScope(symbol)) RuntimeError("Global symbol \"" + symbol + "\" does not exist (Undefined Symbol)");

    return Symbol(GetGlobalScope(), symbol);
}

Symbol Interpreter::EvalLocalIdWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_LOCAL_ID);
    std::string symbol = node[AST_TAG_ID]->ToString();
    return Symbol(currentScope, symbol);
}

Symbol Interpreter::EvalFormalWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_FORMAL);
    CHANGE_LINE();

    std::string formalName = node[AST_TAG_ID]->ToString();

    if (IsLibFunc(formalName)) RuntimeError("Formal argument \"" + formalName + "\" shadows library function");

    if (LookupCurrentScope(formalName)) RuntimeError("Formal argument \"" + formalName + "\" already defined as a formal");

    return Symbol(currentScope, formalName);
}

Symbol Interpreter::EvalDollarEnvWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_DOLLAR_ENV);
    RuntimeError("Cannot modify $env. No write access");
    assert(false);
}

Symbol Interpreter::EvalDollarLambdaWrite(Object &node) {
    ASSERT_TYPE(AST_TAG_DOLLAR_LAMBDA);
    RuntimeError("Cannot modify $lambda. No write access");
    assert(false);
}

Symbol Interpreter::TableSetElem(const Value &lvalue, const Value &index) {
    if (!index.IsString() && !index.IsNumber())
        RuntimeError("Keys of objects can only be strings or numbers. Found " + index.GetTypeToString());

    if (!lvalue.IsObject() && !lvalue.IsProgramFunction())
        RuntimeError("Cannot set field \"" + (index.IsString() ? index.ToString() : std::to_string(index.ToNumber())) + "\" of something that is not an object. Found " + lvalue.GetTypeToString());

    if (index.IsString() &&
        index.ToString()[0] == '$' &&
        !IsReservedField(index.ToString()))
        RuntimeError("Cannot write to field \"" + index.ToString() + "\". No write access to user-defined $ indices");

    /* The 3 valid cases of a TABLESETELEM instruction are:
     *
     * func.x = 0;
     * func.$closure.x = 0;
     * table.x = ;
     */
    if (lvalue.IsProgramFunction())
        return ClosureSetElem(lvalue.ToProgramFunctionClosure_NoConst(), index);
    else if (lvalue.IsObjectClosure())
        return ClosureSetElem(lvalue, index);
    else
        return ObjectSetElem(lvalue, index);
}

Symbol Interpreter::EvalClosureWrite(Object &node) {
    assert(node.IsValid());
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_DOT ||
           node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_BRACKET);

    Symbol symbol = EVAL_WRITE(AST_TAG_LVALUE);

    CHANGE_LINE();

    symbol.SetIsClosureChange(true);
    return symbol;
}

/****** Evaluation Helpers ******/

const Value Interpreter::TableGetElem(const Value &lvalue, const Value &index) {
    // Special case: String indexing
    if (lvalue.IsString()) return StringGetChar(lvalue, index);

    if (!index.IsString() && !index.IsNumber()) RuntimeError("Keys of objects can only be strings or numbers");
    if (!lvalue.IsObject() && !lvalue.IsProgramFunction()) RuntimeError("Cannot get field \"" + (index.IsString() ? index.ToString() : std::to_string(index.ToNumber())) + "\" of something that is not an object. Found " + lvalue.GetTypeToString());

    if (lvalue.IsProgramFunction() &&
        index.IsString() &&
        index.ToString() == CLOSURE_RESERVED_FIELD)
        return Value(lvalue.ToProgramFunctionClosure_NoConst(), true);

#ifdef FUNCTION_AST_FIELD
    if (lvalue.IsProgramFunction() &&
        index.IsString() &&
        index.ToString() == FUNCTION_AST_RESERVED_FIELD)
        return lvalue.ToProgramFunctionAST_NoConst();
#endif

    Object *table = nullptr;
    if (lvalue.IsObject())
        table = lvalue.ToObject_NoConst();
    else
        table = lvalue.ToProgramFunctionClosure_NoConst();

    bool shouldFail = false;
#ifdef CLOSURE_LOOKUP_FAIL_ERROR
    if (lvalue.IsProgramFunction())
        shouldFail = true;
    else if (lvalue.IsObject() && lvalue.IsObjectClosure())
        shouldFail = true;
#endif

    if (index.IsString())
        return GetStringFromContext(table, index, shouldFail);
    else if (index.IsNumber())
        return GetNumberFromContext(table, index, shouldFail);
    else
        assert(false);
}

const Value Interpreter::StringGetChar(const Value &lvalue, const Value &index) {
    assert(lvalue.IsValid() && lvalue.IsString());
    assert(index.IsValid());

    if (!index.IsNumber()) RuntimeError("Only numeric indices allowed for strings");

    if (!Utilities::IsInt(index.ToNumber())) RuntimeError("Only integer indices allowed for strings");

    if (index.ToNumber() >= lvalue.ToString().size()) RuntimeError("Index larger than string size");

    unsigned pos = static_cast<unsigned>(index.ToNumber());

    std::string result = "";
    result += lvalue.ToString()[pos];
    return result;
}

const Value Interpreter::GetIdName(const Object &node) {
    assert(node.ElementExists(AST_TAG_ID));
    const Value *name = node[AST_TAG_ID];
    assert(name->IsString());
    return *name;
}

const Value Interpreter::HandleAggregators(Object &node, MathOp op, bool returnChanged) {
    assert(node.IsValid());

    Symbol lvalue = EVAL_WRITE(AST_TAG_CHILD);
    assert(lvalue.IsValid());

    CHANGE_LINE();

    const Value *value = nullptr;
    if (lvalue.IsIndexString())
        value = (*lvalue.GetContext())[lvalue.ToString()];
    else if (lvalue.IsIndexNumber())
        value = (*lvalue.GetContext())[lvalue.ToNumber()];
    else
        assert(false);

    if (!value && lvalue.IsIndexString())
        RuntimeError("Field \"" + lvalue.ToString() + "\" does not exist in context");

    if (!value && lvalue.IsIndexNumber())
        RuntimeError("Field \"" + std::to_string(lvalue.ToNumber()) + "\" does not exist in context");

    if (!value->IsNumber())
        RuntimeError("Increment/decrement operators can only be applied to numbers not to " + value->GetTypeToString());

    double number = value->ToNumber();
    double result = number;

    if (op == MathOp::Plus)
        result = number + 1;
    else if (op == MathOp::Minus)
        result = number - 1;
    else
        assert(false);

    if (lvalue.IsIndexString())
        lvalue.GetContext()->Set(lvalue.ToString(), result);
    else if (lvalue.IsIndexNumber())
        lvalue.GetContext()->Set(lvalue.ToNumber(), result);
    else
        assert(false);

    if (returnChanged)
        return result;
    else
        return number;
}

const Value Interpreter::EvalMath(Object &node, MathOp op) {
    assert(node.IsValid());

    auto op1 = EVAL(AST_TAG_FIRST_EXPR);
    auto op2 = EVAL(AST_TAG_SECOND_EXPR);

    CHANGE_LINE();

    /* Special case for string concatenation */
    if (op1.IsString() && op2.IsString() && op == MathOp::Plus)
        return (op1.ToString() + op2.ToString());

    if (!op1.IsNumber()) RuntimeError("First operand is not a number in an arithmetic operation. Found " + op1.GetTypeToString());
    if (!op2.IsNumber()) RuntimeError("Second operand is not a number in an arithmetic operation. Found " + op2.GetTypeToString());

    if ((op == MathOp::Div || op == MathOp::Mod) &&
        Utilities::IsZero(op2.ToNumber())) RuntimeError("Cannot divide by zero");

    switch (op) {
        case MathOp::Plus: return op1.ToNumber() + op2.ToNumber();
        case MathOp::Minus: return op1.ToNumber() - op2.ToNumber();
        case MathOp::Mul: return op1.ToNumber() * op2.ToNumber();
        case MathOp::Div: return op1.ToNumber() / op2.ToNumber();
        case MathOp::Greater: return op1.ToNumber() > op2.ToNumber();
        case MathOp::Less: return op1.ToNumber() < op2.ToNumber();
        case MathOp::GreaterEqual: return op1.ToNumber() >= op2.ToNumber();
        case MathOp::LessEqual: return op1.ToNumber() <= op2.ToNumber();
        case MathOp::Mod: {
            double val = static_cast<unsigned>(op1.ToNumber()) % static_cast<unsigned>(op2.ToNumber());
            return val;
        };
        default: assert(false);
    }
}

bool Interpreter::ValuesAreEqual(const Value &v1, const Value &v2) {
    assert(v1.IsValid());
    assert(v2.IsValid());

    /* We do not allow comparisons with Undef */
    if (v1.IsUndef() || v2.IsUndef()) RuntimeError("Undefined value found in equality operator");
    /* If one of the operands is boolean simply convert the other operand to
     * boolean and compare them. */
    if (v1.IsBoolean() || v2.IsBoolean()) return (static_cast<bool>(v1) == static_cast<bool>(v2));
    /* If one of the operands is nil and the other operand is not boolean then
     * the result is true only if both operands are nil */
    if (v1.IsNil() || v2.IsNil()) return (v1.IsNil() && v2.IsNil());
        /* In any other case the operands must have the same type */
#ifdef NO_DIFFERENT_TYPE_COMPARISON
    if (v1.GetType() != v2.GetType()) RuntimeError("Cannot compare operands of different types (" + v1.GetTypeToString() + " and " + v2.GetTypeToString() + ")");
#else
    if (v1.GetType() != v2.GetType()) return false;
#endif

    /* Compare based on type of operands */
    if (v1.IsNumber())
        return Utilities::DoublesAreEqual(v1.ToNumber(), v2.ToNumber());
    else if (v1.IsString())
        return v1.ToString() == v2.ToString();
    else if (v1.IsProgramFunction())
        return v1.ToProgramFunctionAST() == v2.ToProgramFunctionAST();
    else if (v1.IsLibraryFunction())
        return v1.ToLibraryFunction() == v2.ToLibraryFunction();
    else if (v1.IsNativePtr())
        return v1.ToNativePtr() == v2.ToNativePtr();
    else if (v1.IsObject())
        return v1.ToObject() == v2.ToObject();
    else
        assert(false);
}

void Interpreter::AssignToContext(const Symbol &lvalue, const Value &rvalue) {
    assert(lvalue.IsValid());
    assert(rvalue.IsValid());

    const Value *old = nullptr;
    Object *object = nullptr;
    Object *context = lvalue.GetContext();
    assert(context);

    /* Note that access to the old value must come before setting a new value
     * because the old one might get deleted in the Set method. Also its
     * reference counter should be decreased only after the rvalue's reference
     * counter has been increased to avoid a wrong deletion. e.g:
     * obj = [];
     * obj = [ { "x" : obj }];
     * */

    if (lvalue.IsIndexString())
        old = (*context)[lvalue.ToString()];
    else if (lvalue.IsIndexNumber())
        old = (*context)[lvalue.ToNumber()];
    else
        assert(false);

    if (old && old->IsObject())
        object = old->ToObject_NoConst();
    else if (old && old->IsProgramFunction())
        object = old->ToProgramFunctionClosure_NoConst();

    if (lvalue.IsIndexString())
        context->Set(lvalue.ToString(), rvalue);
    else if (lvalue.IsIndexNumber())
        lvalue.GetContext()->Set(lvalue.ToNumber(), rvalue);
    else
        assert(false);

    if (rvalue.IsObject())
        rvalue.ToObject_NoConst()->IncreaseRefCounter();
    else if (rvalue.IsProgramFunction())
        rvalue.ToProgramFunctionClosure_NoConst()->IncreaseRefCounter();

    if (object) object->DecreaseRefCounter();
}

void Interpreter::RemoveFromContext(const Symbol &lvalue, const Value &rvalue) {
    assert(lvalue.IsValid());
    assert(rvalue.IsValid());

    const Value *old = nullptr;
    if (lvalue.IsIndexString())
        old = lvalue.GetContext()->GetAndRemove(lvalue.ToString());
    else if (lvalue.IsIndexNumber())
        old = lvalue.GetContext()->GetAndRemove(lvalue.ToNumber());
    else
        assert(false);

    if (!old) RuntimeWarning("Field \"" + (lvalue.IsIndexString() ? lvalue.ToString() : std::to_string(lvalue.ToNumber())) + "\" not found in object during deletion");

    if (old && old->IsObject())
        old->ToObject_NoConst()->DecreaseRefCounter();
    else if (old && old->IsProgramFunction())
        old->ToProgramFunctionClosure_NoConst()->DecreaseRefCounter();

    delete old;
}

void Interpreter::ChangeClosure(const Symbol &lvalue, const Value &rvalue) {
    assert(lvalue.IsValid());
    assert(rvalue.IsValid());

    Object *closure = nullptr;
    Object *context = lvalue.GetContext();
    assert(context);

    if (!rvalue.IsObject() && !rvalue.IsNil())
        RuntimeError("Cannot change function closure to " + rvalue.GetTypeToString() + ". Only objects are allowed");

    if (rvalue.IsNil())
        closure = new Object();
    else
        closure = rvalue.ToObject_NoConst();

    closure->IncreaseRefCounter();

    if (lvalue.IsIndexNumber()) {
        Value func = *(*context)[lvalue.ToNumber()];
        assert(func.IsProgramFunction());
        func.ToProgramFunctionClosure_NoConst()->DecreaseRefCounter();
        context->Set(lvalue.ToNumber(), Value(func.ToProgramFunctionAST_NoConst(), closure));
    } else if (lvalue.IsIndexString()) {
        Value func = *(*context)[lvalue.ToString()];
        assert(func.IsProgramFunction());
        func.ToProgramFunctionClosure_NoConst()->DecreaseRefCounter();
        context->Set(lvalue.ToString(), Value(func.ToProgramFunctionAST_NoConst(), closure));
    } else
        assert(false);
}

void Interpreter::CleanupForLoop(Value &elist1, Value &elist2) {
    assert(elist1.IsObject());

    elist1.ToObject_NoConst()->Visit([](const Value &key, const Value &val) {
        if (val.IsObject())
            val.ToObject_NoConst()->DecreaseRefCounter();
        else if (val.IsProgramFunction())
            val.ToProgramFunctionClosure_NoConst()->DecreaseRefCounter();
    });

    elist1.ToObject_NoConst()->Clear();
    delete elist1.ToObject_NoConst();

    if (!elist2.IsObject()) return;
    /* Elist2 can be undef if it has not been evaluated yet:
       for(i = 0; i < 10; ++i) if (i == 0) return;
     */
    elist2.ToObject_NoConst()->Visit([](const Value &key, const Value &val) {
        if (val.IsObject())
            val.ToObject_NoConst()->DecreaseRefCounter();
        else if (val.IsProgramFunction())
            val.ToProgramFunctionClosure_NoConst()->DecreaseRefCounter();
    });

    elist2.ToObject_NoConst()->Clear();
    delete elist2.ToObject_NoConst();
}

void Interpreter::ValidateAssignment(const Symbol &lvalue, const Value &rvalue) {
    assert(lvalue.IsValid());
    assert(rvalue.IsValid());

    if (IS_LIB_FUNC())
        RuntimeError("Cannot modify library function \"" + lvalue.ToString() + "\"");

    if (IS_READ_ONLY_SYMBOL())
        RuntimeError("Cannot write to \"" + lvalue.ToString() + "\". No write access");

#undef NO_WRITE_ACCESS_TO_DOLLAR_IDS
#ifdef NO_WRITE_ACCESS_TO_DOLLAR_IDS
    if (IS_DOLLAR_ID())
        RuntimeError("Cannot write to field \"" + lvalue.ToString() + "\". No write access to $ indices");
#endif

    if (IS_OBJECT_REQUIRING_FIELD())
        RuntimeError("Cannot change \"" + lvalue.ToString() + "\" to " + rvalue.GetTypeToString() + ". Only objects are allowed");
}

bool Interpreter::IsValidTreeTag(TreeHost * host, const std::string & type) {
    assert(host);
    assert(!type.empty());

    std::vector<std::string> tags = host->GetInstalledTags();

    return (std::find(tags.begin(), tags.end(), type) != tags.end());
}

bool Interpreter::IsValidAST(const Object &node) {
    assert(node.IsValid());

    bool result = false;

    result = (node.ElementExists(AST_TAG_TYPE_KEY) &&
              node[AST_TAG_TYPE_KEY]->IsString());

    if (!result) return false;

#ifdef FORCE_AST_ROOT
    result = (node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_PROGRAM &&
              node.ElementExists(AST_TAG_CHILD) &&
              node[AST_TAG_CHILD]->IsObject());

    if (!result) return false;
#endif

    PreOrderTreeHost preHost { new SanityVisitor() };

    if (!IsValidTreeTag(&preHost, node[AST_TAG_TYPE_KEY]->ToString())) return false;

    try {
        preHost.Accept(node);
        result = true;

    } catch (SanityException &e) {
        result = false;
    }

    return result;
}

/****** Evaluation Side Actions ******/

void Interpreter::BlockEnter(void) {
    assert(currentScope == scopeStack.front());
    currentScope = PushNested();
    assert(currentScope == scopeStack.front());
}

void Interpreter::BlockExit(void) {
    assert(currentScope);
    assert(currentScope == scopeStack.front());

    Object *tmp = nullptr;

    bool shouldSlice = currentScope->ElementExists(PREVIOUS_RESERVED_FIELD);
    while (currentScope->ElementExists(PREVIOUS_RESERVED_FIELD)) {
        tmp = currentScope;
        currentScope = (*currentScope)[PREVIOUS_RESERVED_FIELD]->ToObject_NoConst();
        assert(currentScope);
        tmp->DecreaseRefCounter();
    }

    if (!currentScope->ElementExists(OUTER_RESERVED_FIELD))
        RuntimeError("Ill-formed block. Expected an outer field");
    else if (!(*currentScope)[OUTER_RESERVED_FIELD]->IsObject())
        RuntimeError("Ill-formed block. Expected an object as outer field");

    assert(currentScope->ElementExists(OUTER_RESERVED_FIELD));
    PopScope();

    if (shouldSlice) currentScope = PushSlice();

    assert(currentScope == scopeStack.front());
}

void Interpreter::ReplaceNumericChild(const Object* child, const Object* list){
    Object* parent = (*child)[PARENT_RESERVED_FIELD]->ToObject_NoConst();

    unsigned index;
    for (index = 0; index < parent->GetNumericSize(); index++)
        if ((*parent)[index]->ToObject_NoConst() == child)
            break;

    assert(index < parent->GetNumericSize());

    unsigned offset = list->GetNumericSize() - 1;

    for (unsigned i = parent->GetNumericSize() - 1; i > index; i--)
        parent->Set(i + offset, *(*parent)[i]);

    for (unsigned i = 0; i < list->GetNumericSize(); i++){
        (*list)[i]->ToObject_NoConst()->Set(PARENT_RESERVED_FIELD, Value(parent));
        parent->Set(index + i, *(*list)[i]);
    }
}


/* does not increase ref counters and does not set the parent reserved field */
Object* Interpreter::ConstToAst(const Value& value){
    assert(value.IsValid());

    Object *elist = new Object();
    Object *expr = new Object();
    Object *term = new Object();
    Object *primary = new Object();
    Object *const_ = new Object();
    Object *valueNode = new Object();

    switch(value.GetType()){
        case Value::Type::BooleanType:
            if (value.ToBoolean() == true)
                valueNode->Set(AST_TAG_TYPE_KEY, AST_TAG_TRUE);
            else
                valueNode->Set(AST_TAG_TYPE_KEY, AST_TAG_FALSE);
            break;
        case Value::Type::NumberType:
            valueNode->Set(AST_TAG_TYPE_KEY, AST_TAG_NUMBER);
            break;
        case Value::Type::StringType:
            valueNode->Set(AST_TAG_TYPE_KEY, AST_TAG_STRING);
            break;
        case Value::Type::NilType:              assert(false);
        case Value::Type::UndefType:            assert(false);
        case Value::Type::ObjectType:           assert(false);
        case Value::Type::ProgramFunctionType:  assert(false);
        case Value::Type::LibraryFunctionType:  assert(false);
        case Value::Type::NativePtrType:        assert(false);
        default: assert(false);
    }
    valueNode->Set(AST_TAG_VALUE, value);

    elist->Set(AST_TAG_TYPE_KEY, AST_TAG_ELIST);
    expr->Set(AST_TAG_TYPE_KEY, AST_TAG_EXPR);
    term->Set(AST_TAG_TYPE_KEY, AST_TAG_TERM);
    primary->Set(AST_TAG_TYPE_KEY, AST_TAG_PRIMARY);
    const_->Set(AST_TAG_TYPE_KEY, AST_TAG_CONST);

    elist->Set(0, expr);
    expr->Set(AST_TAG_CHILD, term);
    term->Set(AST_TAG_CHILD, primary);
    primary->Set(AST_TAG_CHILD, const_);
    const_->Set(AST_TAG_CHILD, valueNode);

    return elist;
}

bool Interpreter::IsRuntimeEmpty(const Value &value) const{
    if (!value.IsObject())
        return false;

    const Object& o = *(value.ToObject());
    return o[AST_TAG_TYPE_KEY] && o[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_RUNTIME_EMPTY;
}

Object* Interpreter::MetaPlug(Object* place, Object* metaAst, const std::set<std::string> &types){
    switch(metaAst->GetNumericSize()){
        case 0: {
            Object* list = (*place)[PARENT_RESERVED_FIELD]->ToObject_NoConst();
            if (types.find((*list)[AST_TAG_TYPE_KEY]->ToString()) == types.end())
                RuntimeError("Connecting the provided AST to the source AST outputs a non valid AST.");

            Object empty;
            empty.Set(AST_TAG_TYPE_KEY, Value(AST_TAG_RUNTIME_EMPTY));
            empty.Set(PARENT_RESERVED_FIELD, Value((*place)[PARENT_RESERVED_FIELD]->ToObject_NoConst()));
            new (place) Object(empty);

            return place;
        }
        case 1:{
            Object* child = (*metaAst)[0]->ToObject_NoConst();
            child->Set(PARENT_RESERVED_FIELD, Value((*place)[PARENT_RESERVED_FIELD]->ToObject_NoConst()));
            new (place) Object(*child);

            return place;
        }
        default:{
            Object* list = (*place)[PARENT_RESERVED_FIELD]->ToObject_NoConst();
            if (types.find((*list)[AST_TAG_TYPE_KEY]->ToString()) == types.end())
                RuntimeError("Connecting the provided AST to the source AST outputs a non valid AST.");

            ReplaceNumericChild(place, metaAst);

            /* evaluate only the first list node, the rest will be evaluated by EvalStatemtnt / EvalArgumentList / EvalExpressionList */
            return (*metaAst)[0]->ToObject_NoConst();
        }
    }

    assert(false);
    return nullptr;
}

const Value Interpreter::MetaCheckAndClone(const Object &node){
    Object *primary, *term, *expr, *ret, *metaAst;

    CHANGE_LINE();

    primary = node[PARENT_RESERVED_FIELD]->ToObject_NoConst();
    assert((*primary)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_PRIMARY);

    term = (*primary)[PARENT_RESERVED_FIELD]->ToObject_NoConst();
    assert((*term)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_TERM);

    expr = (*term)[PARENT_RESERVED_FIELD]->ToObject_NoConst();
    assert((*expr)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_EXPR);

    Value metaAstVal = EVAL_CHILD();

    if (!metaAstVal.IsObject()){
        if (metaAstVal.IsNil()){
            metaAst = new Object();
            metaAst->Set(AST_TAG_TYPE_KEY, AST_TAG_ELIST);
        }
        else if (metaAstVal.IsBoolean() || metaAstVal.IsNumber() || metaAstVal.IsString())
            metaAst = ConstToAst(metaAstVal);
        else
            RuntimeError("Provided AST is not an object. Found " + metaAstVal.GetTypeToString());
    }else
        metaAst = metaAstVal.ToObject_NoConst();

    if (!IsValidAST(*metaAst)) RuntimeError("Provided AST is not valid");

    PostOrderTreeHost refAndParHost({ new IncreaseRefCounterVisitor(), new SetParentTreeVisitor() });
    refAndParHost.Accept(*metaAst);

    if ((*metaAst)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_PROGRAM)        /* if it's a program get the statements */
        metaAst = (*metaAst)[AST_TAG_CHILD]->ToObject_NoConst();

    const std::string &type = (*metaAst)[AST_TAG_TYPE_KEY]->ToString();
    if (type == AST_TAG_STMTS){
        Object* statement = (*expr)[PARENT_RESERVED_FIELD]->ToObject_NoConst();
        if ((*statement)[AST_TAG_TYPE_KEY]->ToString() != AST_TAG_STMT)
            RuntimeError("Connecting the provided AST to the source AST outputs a non valid AST. Expected statement but found " + (*statement)[AST_TAG_TYPE_KEY]->ToString());

        ret = MetaPlug(statement, metaAst, {AST_TAG_STMTS});
    }else if (type == AST_TAG_ELIST)
        ret = MetaPlug(expr, metaAst, {AST_TAG_ELIST, AST_TAG_ARGLIST});
    else
        RuntimeError("Provided ASTs should be whole programs or begin with an elist / stmts node");

    /* Validate the new AST (break,continue,return) */
    PostOrderTreeHost validityHost(new ValidityVisitor());
    validityHost.Accept(*metaAst);

    RenameAnonymousFuncs(metaAst);

    return Value(ret);
}

/****** Function Call Evaluation Helpers ******/

Value Interpreter::CallProgramFunction(Object *functionClosure, const Object *arguments, const Object *formals, Object *body, const Object *evalArguments, const Value &lvalue) {
    assert(currentScope == scopeStack.front());
    ProgramFunctionRuntimeChecks(arguments, formals, lvalue);

    //function entry
    currentScope = PushScopeSpace(functionClosure);
    inFunctionScope = true;

    ProgramFunctionArgumentsToFormalsResolver(arguments, formals, evalArguments, lvalue);
    // ProgramFunctionArgumentsIncreaseRefCounters(evalArguments, lvalue); //Done in EvalArgumentList

    //function body evaluation
    try {
        dispatcher.Eval(*body);
        SetRetvalRegister(Value());
    } catch (const ReturnException &e) {
        ;
    }

    //function exit
    //PopScope(); Done in BlockExit
    PopScopeSpace();
    currentScope = scopeStack.front();

    return retvalRegister;
}

Value Interpreter::CallLibraryFunction(const std::string &functionId, LibraryFunc functionLib, Object *env, const Value &lvalue) {
    assert(functionLib);
    assert(!functionId.empty());

    //Handle lvalue
    if (!lvalue.IsUndef()) InsertToFront(env, lvalue);

    env->Set(RETVAL_RESERVED_FIELD, Value(NilTypeValue::Nil));

    functionLib(*env);    //Library function Call
    const Value *result = (*env)[RETVAL_RESERVED_FIELD];

    SetRetvalRegister(*result);

    LibraryFunctionEnvDecreaseRefCounters(env);

    return retvalRegister;
}

void Interpreter::ProgramFunctionRuntimeChecks(const Object *arguments, const Object *formals, const Value &lvalue) {
    std::vector<std::string> argumentNamesVec = GetArgumentNames(arguments);
    std::vector<std::string> formalNamesVec = GetFormalNames(formals);
    std::set<std::string> argumentNames(argumentNamesVec.begin(), argumentNamesVec.end());
    std::set<std::string> formalNames(formalNamesVec.begin(), formalNamesVec.end());
    unsigned positionalArgSize = GetPositionalArgSize(arguments);
    if (!lvalue.IsUndef()) positionalArgSize++;

    //Number of args error
    unsigned formalSize = formals->GetNumericSize();
    if (positionalArgSize > formalSize)
        RuntimeWarning("Number of actual positional arguments(" + std::to_string(positionalArgSize) + ") exceeds number of formal parameters(" + std::to_string(formalSize) + ")");

    //Unexpected named error (name not defined in formals)
    std::vector<std::string> actualsMinusFormals;
    std::set_difference(argumentNames.begin(), argumentNames.end(),
                        formalNames.begin(), formalNames.end(),
                        std::inserter(actualsMinusFormals, actualsMinusFormals.begin()));
    if (!actualsMinusFormals.empty()) {
        std::stringstream s;
        std::copy(actualsMinusFormals.begin(), actualsMinusFormals.end(), std::ostream_iterator<std::string>(s, ","));
        std::string msg(s.str());
        msg.pop_back();
        if (actualsMinusFormals.size() == 1)
            msg = "Formal " + msg + " is not defined";
        else
            msg = "Formals " + msg + " are not defined";
        RuntimeError(msg);
    }

    //checks on actual-formal matching
    for (register unsigned i = 0; i < formals->GetNumericSize(); ++i) {
        const Object *formal = (*formals)[i]->ToObject();
        std::string formalName = formalNamesVec.at(i);    //Name of formal, i is index of formal/actual
        bool matchExists = false;                         //actual-formal match exists

        if (i < positionalArgSize) {
            //Both positional and named actual match error
            if (argumentNames.find(formalName) != argumentNames.end())
                RuntimeError("Both positional and named match on formal parameter " + formalName + " exists");
            matchExists = true;
        } else if (argumentNames.find(formalName) != argumentNames.end()) {
            matchExists = true;
        }

        if ((*formal)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FORMAL) {
            //A non-optional formal is not matched
            if (!matchExists)
                RuntimeWarning("Formal parameter " + formalName + " does not match actual argument");
        } else if ((*formal)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ASSIGN) {
            ;
        } else {
            assert(false);
        }
    }
}

void Interpreter::ProgramFunctionArgumentsToFormalsResolver(const Object *arguments, const Object *formals, const Object *evalArguments, const Value &lvalue) {
    std::vector<std::string> formalNames = GetFormalNames(formals);
    unsigned positionalArgSize = GetPositionalArgSize(arguments);
    unsigned formalSize = formals->GetNumericSize();
    unsigned formalOffset = 0;
    //Handle lvalue
    if (!lvalue.IsUndef()) {
        if (formalSize > 0)
            currentScope->Set(formalNames.at(0), lvalue);    // positional arg at string index
        else
            currentScope->Set(0, lvalue);    // extra arg at numerical index
        formalOffset = 1;
    }
    //Match positional args
    for (register unsigned i = 0; i < formalSize && i < positionalArgSize; ++i) {
        std::string name = formalNames.at(i + formalOffset);
        currentScope->Set(name, *((*evalArguments)[i]));    // positional arg at string index
    }
    //Match extra (unmatched) positional args
    for (register unsigned i = formalSize; i < positionalArgSize; i++) {
        currentScope->Set(i - formalSize, *((*evalArguments)[i]));    // extra arg at numerical index
    }
    //Match named args
    for (register unsigned i = positionalArgSize; i < arguments->GetNumericSize(); ++i) {
        const Object *argument = (*arguments)[i]->ToObject();
        std::string name = GetNamedKey(argument);
        assert(std::find(formalNames.begin(), formalNames.end(), name) != formalNames.end());
        currentScope->Set(name, *((*evalArguments)[i]));    // named arg at string index
    }
    //Evaluate and add the optional formals or unmatched(undefined) formals to the current scope (function scope)
    for (register unsigned i = 0; i < formals->GetNumericSize(); ++i) {
        if (currentScope->ElementExists(formalNames.at(i))) continue;
        Object *formal = (*formals)[i]->ToObject_NoConst();
        assert((*formal)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FORMAL || (*formal)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ASSIGN);
        dispatcher.Eval(*formal);
    }
}

void Interpreter::LibraryFunctionEnvDecreaseRefCounters(const Object *env) {
    for (register unsigned i = 0; i < env->GetNumericSize(); ++i) {
        const Value v = *((*env)[i]);
        if (v.IsObject())
            v.ToObject_NoConst()->DecreaseRefCounter();
        else if (v.IsProgramFunction())
            v.ToProgramFunctionClosure_NoConst()->DecreaseRefCounter();
    }
}

std::vector<std::string> Interpreter::GetArgumentNames(const Object *arguments) {
    std::vector<std::string> names;
    for (register unsigned i = 0; i < arguments->GetNumericSize(); ++i) {
        const Object *argument = (*arguments)[i]->ToObject();
        std::string name;
        if ((*argument)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_NAMED) {
            assert(argument->ElementExists(AST_TAG_NAMED_KEY));
            const Object *keyNode = (*argument)[AST_TAG_NAMED_KEY]->ToObject();
            assert(keyNode->ElementExists(AST_TAG_ID));
            name = (*keyNode)[AST_TAG_ID]->ToString();
            names.push_back(name);
        }
    }
    return names;
}

std::vector<std::string> Interpreter::GetFormalNames(const Object *formals) {
    std::vector<std::string> named;
    for (register unsigned i = 0; i < formals->GetNumericSize(); ++i) {
        const Object *formal = (*formals)[i]->ToObject();
        assert(((*formal)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FORMAL) || ((*formal)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ASSIGN));
        std::string name;
        if ((*formal)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FORMAL) {
            assert(formal->ElementExists(AST_TAG_ID));
            name = (*formal)[AST_TAG_ID]->ToString();
        } else if ((*formal)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ASSIGN) {
            assert(formal->ElementExists(AST_TAG_LVALUE));
            const Object *formalNode = (*formal)[AST_TAG_LVALUE]->ToObject();
            assert((*formalNode)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FORMAL);
            assert(formalNode->ElementExists(AST_TAG_ID));
            name = (*formalNode)[AST_TAG_ID]->ToString();
        } else {
            assert(false);
        }
        named.push_back(name);
    }
    return named;
}

unsigned Interpreter::GetPositionalArgSize(const Object *arguments) {
    unsigned i = 0;
    for (i = 0; i < arguments->GetNumericSize(); ++i) {
        const Object *argument = (*arguments)[i]->ToObject();
        if ((*argument)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_NAMED)
            return i;
    }
    return i;
}

std::string Interpreter::GetNamedKey(const Object *argument) {
    assert((*argument)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_NAMED);
    const Object *idNode = (*argument)[AST_TAG_NAMED_KEY]->ToObject();
    assert(idNode->ElementExists(AST_TAG_ID));
    std::string name = (*idNode)[AST_TAG_ID]->ToString();
    return name;
}

void Interpreter::InsertToFront(Object *obj, const Value &lvalue) {
    for (int i = obj->GetNumericSize(); i > 0; i--)
        obj->Set(i, *((*obj)[i - 1]));    //Shift to the right
    obj->Set(0, lvalue);
}

/****** Symbol Lookup ******/

const Value *Interpreter::LookupScope(Object *scope, const std::string &symbol) const {
    assert(scope && scope->IsValid());
    assert(!symbol.empty());

    Object *target = SymbolLookup(scope, symbol);
    if (target) {
        assert(target->ElementExists(symbol));
        return (*target)[symbol];
    }

    return nullptr;
}

const Value *Interpreter::LookupCurrentScope(const std::string &symbol) const {
    assert(!symbol.empty());
    return LookupScope(currentScope, symbol);
}

const Value *Interpreter::LookupGlobalScope(const std::string &symbol) const {
    assert(!symbol.empty());

    Object *scope = GetGlobalScope();
    return LookupScope(scope, symbol);
}

Object *Interpreter::SymbolLookup(Object *scope, const std::string &scopeKey, const std::string &symbol) const {
    assert(scope);
    assert(!scopeKey.empty());
    assert(!symbol.empty());

    if (auto inner = (*scope)[scopeKey]) {
        assert(inner->IsObject());
        if (auto val = SymbolLookup(inner->ToObject_NoConst(), symbol)) return val;
    }

    return nullptr;
}

Object *Interpreter::SymbolLookup(Object *scope, const std::string &symbol) const {
    assert(scope);
    assert(!symbol.empty());

    Object *val = nullptr;

    if (scope->ElementExists(symbol))
        return scope;
    else if ((val = SymbolLookup(scope, LOCAL_RESERVED_FIELD, symbol)))
        return val;
    else if ((val = SymbolLookup(scope, PREVIOUS_RESERVED_FIELD, symbol)))
        return val;
    else if ((val = SymbolLookup(scope, OUTER_RESERVED_FIELD, symbol)))
        return val;
    else
        return nullptr;
}

Object *Interpreter::FindScope(const std::string &symbol) const {
    assert(!symbol.empty());
    assert(currentScope);

    return SymbolLookup(currentScope, symbol);
}

Object *Interpreter::GetGlobalScope(void) const {
    assert(!scopeStack.empty());
    assert(currentScope == scopeStack.front());

    Object *global = scopeStack.back();
    assert(global);

    while (global->ElementExists(OUTER_RESERVED_FIELD)) {
        const Value *val = (*global)[OUTER_RESERVED_FIELD];

        if (!val->IsObject()) RuntimeError("Ill-formed block. Expected an object as outer field");

        global = val->ToObject_NoConst();
        assert(global);
    }

    return global;
}

bool Interpreter::IsGlobalScope(Object *scope) const {
    assert(scope);
    assert(currentScope == scopeStack.front());

    /* Move backwards towards the first slice of the scope */
    while (scope->ElementExists(PREVIOUS_RESERVED_FIELD)) {
        scope = (*scope)[PREVIOUS_RESERVED_FIELD]->ToObject_NoConst();
    }

    /* The first slice of the global scope should contain library functions */
    return (!scope->ElementExists(OUTER_RESERVED_FIELD) &&
            scope == globalScope &&
            scope->ElementExists("print") &&
            (*scope)["print"]->IsLibraryFunction());
}

/****** Environment Actions ******/

Object *Interpreter::PushScopeSpace(Object *outerScope) {
    assert(outerScope);    // This can only be NULL in the case of global scope
    if (scopeStack.size() >= MAX_STACK_SIZE) RuntimeError("Stack Overflow");
    Object *newScopeSpace = new Object();
    newScopeSpace->Set(OUTER_RESERVED_FIELD, outerScope);
    newScopeSpace->IncreaseRefCounter();
    if (outerScope) outerScope->IncreaseRefCounter();    //remove if clause since we assert?
    scopeStack.push_front(newScopeSpace);
    return newScopeSpace;
}

void Interpreter::PopScopeSpace(void) {
    assert(!scopeStack.empty());
    scopeStack.pop_front();
}

Object *Interpreter::PushSlice(void) {
    return PushScope(PREVIOUS_RESERVED_FIELD);
}

Object *Interpreter::PushNested(void) {
    return PushScope(OUTER_RESERVED_FIELD);
}

Object *Interpreter::PopScope(void) {
    assert(currentScope && currentScope->IsValid());
    assert(currentScope->ElementExists(OUTER_RESERVED_FIELD));

    Object *scope = currentScope;
    currentScope = (*scope)[OUTER_RESERVED_FIELD]->ToObject_NoConst();

    scope->DecreaseRefCounter();
    scopeStack.pop_front();
    scopeStack.push_front(currentScope);
    return scope;
}

Object *Interpreter::PushScope(const std::string &tag) {
    assert(!tag.empty());
    assert(tag == PREVIOUS_RESERVED_FIELD ||
           tag == OUTER_RESERVED_FIELD);
    assert(currentScope);

    Object *scope = new Object();
    scope->Set(tag, Value(currentScope));

    currentScope->IncreaseRefCounter();
    scope->IncreaseRefCounter();

    assert(!scopeStack.empty());
    scopeStack.pop_front();
    scopeStack.push_front(scope);

    return scope;
}

/****** Helpers ******/

bool Interpreter::IsLibFunc(const std::string &symbol) const {
    assert(!symbol.empty());
    return std::find(libraryFuncs.begin(), libraryFuncs.end(), symbol) != libraryFuncs.end();
}

bool Interpreter::IsReservedField(const std::string &index) const {
    return (index == PARENT_RESERVED_FIELD ||
            index == UNPARSE_VALUE_RESERVED_FIELD ||
            index == CLONED_TREE_RESERVED_FIELD ||
            index == PREVIOUS_RESERVED_FIELD ||
            index == OUTER_RESERVED_FIELD ||
            index == ENV_RESERVED_FIELD ||
            index == LAMBDA_RESERVER_FIELD ||
            index == ENV_RESERVED_FIELD ||
            index == LOCAL_RESERVED_FIELD ||
            index == RETVAL_RESERVED_FIELD ||
            index == CLOSURE_RESERVED_FIELD ||
            index == LINE_NUMBER_RESERVED_FIELD);
}

const Value Interpreter::GetStringFromContext(Object *table, const Value &index, bool lookupFail) {
    assert(table && table->IsValid());
    assert(index.IsString());

    std::string str = index.ToString();

    bool elementExists = table->ElementExists(str);

    if (!elementExists && lookupFail)
        RuntimeError("Field \"" + str + "\" does not exist");
    else if (!elementExists)
        return Value(NilTypeValue::Nil);
    else
        return *(*table)[str];

    assert(false); /* Keep this to suppress -Wreturn-type warning */
}

const Value Interpreter::GetNumberFromContext(Object *table, const Value &index, bool lookupFail) {
    assert(table && table->IsValid());
    assert(index.IsNumber());

    double num = index.ToNumber();

    bool elementExists = table->ElementExists(num);

    if (!elementExists && lookupFail)
        RuntimeError("Field " + std::to_string(num) + " does not exist");
    else if (!elementExists)
        return Value(NilTypeValue::Nil);
    else
        return *(*table)[num];

    assert(false); /* Keep this to suppress -Wreturn-type warning */
}

Symbol Interpreter::ClosureSetElem(const Value &lvalue, const Value &index) {
    assert(lvalue.IsObject());

    Object *table = lvalue.ToObject_NoConst();
    assert(table);

#ifdef CLOSURE_LOOKUP_FAIL_ERROR
    if (index.IsNumber() && !table->ElementExists(index.ToNumber()))
        RuntimeError("Cannot set field \"" + std::to_string(index.ToNumber()) + "\". It does not exist in closure.");
    else if (index.IsString() && !table->ElementExists(index.ToString()))
        RuntimeError("Cannot set field \"" + index.ToString() + "\". It does not exist in closure.");
#endif

    if (index.IsNumber())
        return Symbol(table, index.ToNumber(), SYMBOL_IS_TABLE);
    else if (index.IsString())
        return Symbol(table, index.ToString(), SYMBOL_IS_TABLE);
    else
        assert(false);
}

Symbol Interpreter::ObjectSetElem(const Value &lvalue, const Value &index) {
    assert(lvalue.IsObject());

    Object *table = lvalue.ToObject_NoConst();
    assert(table);

    if (index.IsNumber())
        return Symbol(table, index.ToNumber(), SYMBOL_IS_TABLE);
    else if (index.IsString())
        return Symbol(table, index.ToString(), SYMBOL_IS_TABLE);
    else
        assert(false);
}

unsigned Interpreter::GetLineNumber(void) const {
    assert(lineNumber);
    return lineNumber;
}

void Interpreter::SetLineNumber(unsigned num) {
    assert(num);
    lineNumber = num;
}

void Interpreter::SetRetvalRegister(const Value &result) {
    if (result.IsObject())
        result.ToObject_NoConst()->IncreaseRefCounter();
    else if (result.IsProgramFunction())
        result.ToProgramFunctionClosure_NoConst()->IncreaseRefCounter();

    if (retvalRegister.IsObject())
        retvalRegister.ToObject_NoConst()->DecreaseRefCounter();
    else if (retvalRegister.IsProgramFunction())
        retvalRegister.ToProgramFunctionClosure_NoConst()->DecreaseRefCounter();

    // I don't think we need to do something about function closures
    retvalRegister = result;
}

void Interpreter::CleanUpGlobalScope(void) {
    assert(currentScope);
    assert(currentScope == scopeStack.front());
    assert(scopeStack.size() == 1);

#ifdef FULL_CLEANUP
    Object *scope = scopeStack.front();
    Object *prev = nullptr;

    while (scope) {
        assert(!scope->ElementExists(OUTER_RESERVED_FIELD));

        const Value *value = (*scope)[PREVIOUS_RESERVED_FIELD];

        if (value)
            prev = value->ToObject_NoConst();
        else
            prev = nullptr;

        while(scope->GetReferences()) scope->DecreaseRefCounter();

        scope = prev;
    }
#else
    globalScope->Clear();
    delete globalScope;
#endif
}

void Interpreter::RenameAnonymousFuncs(Object * metaAst) {
    assert(metaAst);
    assert(metaAst->IsValid());

    AnonymousFuncsVisitor * anonymousHanlder = new AnonymousFuncsVisitor();
    anonymousHanlder->SetNameOffset(anonymousFuncOffset);

    PostOrderTreeHost funcsHost(anonymousHanlder);
    funcsHost.Accept(*metaAst);

    anonymousFuncOffset = anonymousHanlder->GetNameOffset();
}

void Interpreter::VisualizeTree(const Object * ast, const std::string & filename) {
    assert(ast);
    assert(ast->IsValid());

    VisualizeVisitor * visitor = new VisualizeVisitor(filename);
    PostOrderTreeHost host(visitor);

    host.Accept(*ast);
    visitor->WriteFile();
}