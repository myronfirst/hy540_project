#include "LibraryFunctions.h"
#include "Deallocator.h"
#include "Interpreter.h"
#include "PostOrderTreeHost.h"
#include "SetParentTreeVisitor.h"
#include "TreeTags.h"
#include "Utilities.h"
#include "ValidityVisitor.h"
#include "IncreaseRefCountVisitor.h"
#include "parser.h"

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <sstream>
#include <thread>

unsigned whitespace = 0;

#define BUFFER_SIZE 1024
#define WHITESPACE_STEP 4
#define NO_NEWLINE (false)

#define NAT_POINTER_EXTERNAL_FILE "EXTERNAL_FILE_PTR"
#define SET_RETVAL(x) env.Set(RETVAL_RESERVED_FIELD, (x));
#define THROW_WRONG_ARGUMENT Interpreter::GetInstance()->RuntimeError("Wrong Argument Value");
#define PRINT(x) std::cout << (x) << std::endl

#define NUM_FUNC_WRONG_ARG(func, type) \
    Interpreter::GetInstance()->RuntimeError("Library function \"" func "\" received " + type + " instead of a number")

#define STR_FUNC_WRONG_ARG(func, type) \
    Interpreter::GetInstance()->RuntimeError("Library function \"" func "\" received " + type + " instead of a string")

#define NUM_FUNC_VALIDATE_AND_RETURN(func, result)                                                                                          \
    if (std::isnan(result) || std::isinf(result)) {                                                                                         \
        Interpreter::GetInstance()->RuntimeWarning("Library function \"" func "\" cannot compute a result. Numeric behavior is undefined"); \
        SET_RETVAL(Value());                                                                                                                \
    } else                                                                                                                                  \
        SET_RETVAL(result);

void PrintNumber(std::ostream &stream, const Value *value, bool newline = true);
void PrintString(std::ostream &stream, const Value *value, bool newline = true);
void PrintBoolean(std::ostream &stream, const Value *value, bool newline = true);
void PrintUndef(std::ostream &stream, const Value *value, bool newline = true);
void PrintNil(std::ostream &stream, const Value *value, bool newline = true);
void PrintLibraryFunction(std::ostream &stream, const Value *value, bool newline = true);
void PrintNativePointer(std::ostream &stream, const Value *value, bool newline = true);
void PrintProgramFunction(std::ostream &stream, const Value *value, bool newline = true);
void PrintObject(std::ostream &stream, const Value *value, bool newline = true);
void PrintValue(std::ostream &stream, const Value *value, bool newline = true);

const Value *GetArgument(Object &env, unsigned argNo, const std::string &optArgName = "") {
    assert(env.IsValid());

    if (optArgName.empty()) {
        if (!env[argNo]) Interpreter::GetInstance()->RuntimeError("Library function did not receive argument " + std::to_string(argNo + 1));
        return env[argNo];
    }

    const Value *arg = env[optArgName];
    if (!arg) arg = env[argNo];

    Interpreter::GetInstance()->RuntimeError("Library function did not receive valid argument");

    return arg;
}

void PrintNumber(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsNumber());
    stream << (Utilities::IsInt(value->ToNumber()) ? static_cast<int>(value->ToNumber()) : value->ToNumber());
    if (newline) stream << std::endl;
}

void PrintString(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsString());
    stream << value->ToString();
    if (newline) stream << std::endl;
}

void PrintBoolean(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsBoolean());
    stream << (value->ToBoolean() ? "true" : "false");
    if (newline) stream << std::endl;
}

void PrintUndef(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsUndef());
    stream << "undef";
    if (newline) stream << std::endl;
}

void PrintNil(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsNil());
    stream << "nil";
    if (newline) stream << std::endl;
    ;
}

void PrintLibraryFunction(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsLibraryFunction());
    stream << "[ Library Function \"" << value->ToLibraryFunctionId() << "\" ]";
    if (newline) stream << std::endl;
}

void PrintNativePointer(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsNativePtr());
    stream << "[ Pointer " << value->ToNativeTypeId() << " ]";
    if (newline) stream << std::endl;
}

void PrintProgramFunction(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsProgramFunction());

    const Object *funcdef = value->ToProgramFunctionAST();
    assert((*funcdef)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FUNCTION_DEF);

    const Object *child = (*funcdef)[AST_TAG_FUNCTION_ID]->ToObject();
    assert((*child)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ID);

    std::string name = (*child)[AST_TAG_ID]->ToString();

    if (name[0] == '$')
        stream << "[ Anonymous Function ]";
    else
        stream << "[ Function \"" + name + "\" ]";
    if (newline) stream << std::endl;
}

void PrintObject(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsObject());

    Object *o = value->ToObject_NoConst();

    if (o->GetTotal() == 0) {
        stream << "[Object] [ ]";
        if (newline) stream << std::endl;
        return;
    }

    stream << "[Object] [" << std::endl;

    whitespace += WHITESPACE_STEP;
    o->Visit([&stream, newline](const Value &key, const Value &val) {
        if (key.IsString())
            stream << std::setw(whitespace) << '"' << key.ToString() << "\" : ";
        else if (key.IsNumber())
            stream << std::setw(whitespace) << key.ToNumber() << " : ";
        else
            assert(false);

        PrintValue(stream, &val, newline);
    });
    whitespace -= WHITESPACE_STEP;

    stream << std::setw(whitespace) << "]";
    if (newline) stream << std::endl;
}

void PrintValue(std::ostream &stream, const Value *value, bool newline) {
    assert(stream.good());
    assert(value && value->IsValid());

    switch (value->GetType()) {
        case Value::Type::NumberType: return PrintNumber(stream, value, newline);
        case Value::Type::StringType: return PrintString(stream, value, newline);
        case Value::Type::BooleanType: return PrintBoolean(stream, value, newline);
        case Value::Type::UndefType: return PrintUndef(stream, value, newline);
        case Value::Type::NilType: return PrintNil(stream, value, newline);
        case Value::Type::LibraryFunctionType: return PrintLibraryFunction(stream, value, newline);
        case Value::Type::NativePtrType: return PrintNativePointer(stream, value, newline);
        case Value::Type::ProgramFunctionType: return PrintProgramFunction(stream, value, newline);
        case Value::Type::ObjectType: return PrintObject(stream, value, newline);
        default: assert(false);
    }
}

void LibFunc::Print(Object &env) {
    assert(env.IsValid());

    for (register unsigned i = 0; i < env.GetNumericSize(); ++i) {
        const Value *value = GetArgument(env, i);
        PrintValue(std::cout, value);
    }

    SET_RETVAL(Value());
}

void LibFunc::SimplePrint(Object &env) {
    assert(env.IsValid());

    for (register unsigned i = 0; i < env.GetNumericSize(); ++i) {
        const Value *value = GetArgument(env, i);
        PrintValue(std::cout, value, NO_NEWLINE);
    }

    SET_RETVAL(Value());
}

void LibFunc::Typeof(Object &env) {
    assert(env.IsValid());
    const Value *value = GetArgument(env, 0);
    assert(value);
    SET_RETVAL(value->GetTypeToString());
}

void LibFunc::ObjectKeys(Object &env) {
    assert(env.IsValid());

    const Value *value = GetArgument(env, 0);
    assert(value);
    if (!value->IsObject()) return Interpreter::GetInstance()->RuntimeError("Library function \"object_keys\" received a " + value->GetTypeToString() + " instead of an object");

    const Object *obj = value->ToObject();
    Object *table = new Object();
    unsigned index = 0;

    obj->Visit([table, &index](const Value &key, const Value &val) {
        table->Set(index++, key);
    });

    SET_RETVAL(table);
}

void LibFunc::ObjectSize(Object &env) {
    assert(env.IsValid());

    const Value *value = GetArgument(env, 0);
    assert(value);
    if (!value->IsObject()) return Interpreter::GetInstance()->RuntimeError("Library function \"object_size\" received a " + value->GetTypeToString() + " instead of an object");

    const Object *obj = value->ToObject();
    SET_RETVAL(static_cast<double>(obj->GetTotal()));
}

void LibFunc::ObjectIncludes(Object &env) {
    assert(env.IsValid());

    const Value *object = GetArgument(env, 0);
    const Value *index = GetArgument(env, 1);

    assert(object);
    assert(index);

    if (!object->IsObject()) return Interpreter::GetInstance()->RuntimeError("Library function \"object_includes\" received a " + object->GetTypeToString() + " instead of an object");
    if (!index->IsString() && !index->IsNumber()) return Interpreter::GetInstance()->RuntimeError("Library function \"object_includes\" received a " + object->GetTypeToString() + " as index. Only strings and numbers are allowed");

    bool result = false;

    object->ToObject()->Visit([index, &result](const Value &key, const Value &val) {
        /* Compare based on type of operands */
        if (index->GetType() != val.GetType()) return;

        if (val.IsNumber())
            result |= Utilities::DoublesAreEqual(val.ToNumber(), index->ToNumber());
        else if (val.IsString())
            result |= val.ToString() == index->ToString();
        else if (val.IsProgramFunction())
            result |= val.ToProgramFunctionAST() == index->ToProgramFunctionAST();
        else if (val.IsLibraryFunction())
            result |= val.ToLibraryFunction() == index->ToLibraryFunction();
        else if (val.IsNativePtr())
            result |= val.ToNativePtr() == index->ToNativePtr();
        else if (val.IsObject())
            result |= val.ToObject() == index->ToObject();
        else
            assert(false);
    });

    SET_RETVAL(result);
}

void LibFunc::ObjectPush(Object &env) {
    assert(env.IsValid());

    const Value *object = GetArgument(env, 0);
    const Value *value = GetArgument(env, 1);

    assert(object);
    assert(value);

    if (!object->IsObject()) return Interpreter::GetInstance()->RuntimeError("Library function \"object_push\" received a " + object->GetTypeToString() + " instead of an object");
    if (value->IsUndef() || value->IsNil()) return Interpreter::GetInstance()->RuntimeError("Library function \"object_push\" cannot push undef or nil");

    object->ToObject_NoConst()->Set(object->ToObject()->GetNumericSize(), *value);

    SET_RETVAL(Value());
}

void LibFunc::Sleep(Object &env) {
    assert(env.IsValid());

    const Value *value = GetArgument(env, 0);
    assert(value);

    if (!value->IsNumber()) return Interpreter::GetInstance()->RuntimeError("Library function \"sleep\" did not receive a number");
    if (!Utilities::IsInt(value->ToNumber())) return Interpreter::GetInstance()->RuntimeError("Library function \"sleep\" did not receive an integer");

    int number = static_cast<int>(value->ToNumber());

    std::this_thread::sleep_for(std::chrono::milliseconds(number));

    SET_RETVAL(Value());
}

void LibFunc::Assert(Object &env) {
#ifdef ASSERT_MULTIPLE_ARGUMENTS
    for (register unsigned i = 0; i < env.GetNumericSize(); ++i) {
        const Value *value = GetArgument(env, i);
        assert(value);
        if (!value->operator bool()) Interpreter::GetInstance()->Assert("Value of " + value->GetTypeToString() + " was evaluated to false");
    }
#else
    const Value *message = nullptr;
    const Value *value = GetArgument(env, 0);
    assert(value);

    if (env.GetNumericSize() > 1) message = GetArgument(env, 1);
    if (message && !message->IsString()) STR_FUNC_WRONG_ARG("assert", message->GetTypeToString());

    bool result = value->operator bool();

    if (!result && message)
        Interpreter::GetInstance()->Assert(message->ToString());
    else if (!result)
        Interpreter::GetInstance()->Assert("Value of " + value->GetTypeToString() + " was evaluated to false");
#endif
    SET_RETVAL(true);
}

void LibFunc::Sqrt(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (!value->IsNumber()) NUM_FUNC_WRONG_ARG("sqrt", value->GetTypeToString());

    double result = std::sqrt(value->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("sqrt", result);
}

void LibFunc::Pow(Object &env) {
    assert(env.IsValid());
    const auto &value1 = GetArgument(env, 0);
    const auto &value2 = GetArgument(env, 1);
    if (!value1->IsNumber()) NUM_FUNC_WRONG_ARG("pow (1st argument)", value1->GetTypeToString());
    ;
    if (!value2->IsNumber()) NUM_FUNC_WRONG_ARG("pow (2nd argument)", value2->GetTypeToString());
    ;

    double result = std::pow(value1->ToNumber(), value2->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("pow", result);
}

void LibFunc::Sin(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (!value->IsNumber()) NUM_FUNC_WRONG_ARG("sin", value->GetTypeToString());

    double result = std::sin(value->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("sin", result);
}

void LibFunc::Cos(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (!value->IsNumber()) NUM_FUNC_WRONG_ARG("cos", value->GetTypeToString());

    double result = std::cos(value->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("cos", result);
}

void LibFunc::Tan(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (!value->IsNumber()) NUM_FUNC_WRONG_ARG("tan", value->GetTypeToString());

    double result = std::tan(value->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("tan", result);
}

void LibFunc::Floor(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (!value->IsNumber()) NUM_FUNC_WRONG_ARG("floor", value->GetTypeToString());

    double result = std::floor(value->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("floor", result);
}

void LibFunc::Ceil(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (!value->IsNumber()) NUM_FUNC_WRONG_ARG("ceil", value->GetTypeToString());
    ;

    double result = std::ceil(value->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("ceil", result);
}

void LibFunc::Abs(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (!value->IsNumber()) NUM_FUNC_WRONG_ARG("abs", value->GetTypeToString());
    ;

    double result = std::abs(value->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("abs", result);
}

void LibFunc::Round(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (!value->IsNumber()) NUM_FUNC_WRONG_ARG("round", value->GetTypeToString());
    ;

    double result = std::round(value->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("round", result);
}

void LibFunc::Trunc(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (!value->IsNumber()) NUM_FUNC_WRONG_ARG("trunc", value->GetTypeToString());
    ;

    double result = std::trunc(value->ToNumber());
    NUM_FUNC_VALIDATE_AND_RETURN("trunc", result);
}

void LibFunc::GetTime(Object &env) {
    assert(env.IsValid());
    std::time_t t = std::time(0);
    std::tm *now = std::localtime(&t);
    std::string conv = std::to_string(now->tm_mday) + "/" + std::to_string(now->tm_mon) + "/" + std::to_string(1900 + now->tm_year);
    conv += " " + std::to_string(now->tm_hour) + ":" + std::to_string(now->tm_min) + ":" + std::to_string(now->tm_sec);
    SET_RETVAL(conv);
}

bool isNumber(std::string str) {
    unsigned int i = 0;
    if (str[0] == '-') i++;
    while (i < str.length()) {
        if (isdigit(str[i]) == false && str[i] != '.') return false;
        i++;
    }
    return true;
}

void LibFunc::Input(Object &env) {
    assert(env.IsValid());
    std::string input;
    std::getline(std::cin, input);

    if (input == "undef") {
        SET_RETVAL(Value());
    } else if (input == "nil") {
        SET_RETVAL(Value(NIL_VAL));
    } else if (!input.empty() && isNumber(input)) {
        SET_RETVAL(std::stod(input));
    } else {
        SET_RETVAL(input);
    }
}

void SeedRNG(void) {
    unsigned seed = time(NULL);

    std::ifstream urandom("/dev/urandom", std::ios::in | std::ios::binary);
    if (urandom)
        urandom.read(reinterpret_cast<char *>(&seed), sizeof(seed));

    std::srand(seed);
}

void LibFunc::Random(Object &env) {
    assert(env.IsValid());

    static unsigned char callCount = 0;
    if (!callCount) {
        callCount = 1;
        SeedRNG();
    }

    SET_RETVAL(static_cast<double>(std::rand()) / RAND_MAX);
}

void LibFunc::ToNumber(Object &env) {
    assert(env.IsValid());
    const auto &value = GetArgument(env, 0);
    if (value->IsNumber()) {
        SET_RETVAL(value->ToNumber());
        return;
    }
    if (!value->IsString()) THROW_WRONG_ARGUMENT;
    if (value->ToString().empty() || !isNumber(value->ToString())) THROW_WRONG_ARGUMENT;

    SET_RETVAL(std::stod(value->ToString()));
}

void LibFunc::ToString(Object &env) {
    assert(env.IsValid());

    std::stringstream ss;

    const Value *value = GetArgument(env, 0);
    assert(value && value->IsValid());

    if (value->IsUndef())
        ss << "[undef]";
    else if (value->IsNumber())
        ss << value->ToNumber();
    else if (value->IsBoolean() && value->ToBoolean())
        ss << "[true]";
    else if (value->IsBoolean())
        ss << "[false]";
    else if (value->IsString())
        ss << value->ToString();
    else if (value->IsObject())
        ss << "[object]";
    else if (value->IsProgramFunction())
        ss << "[function]";
    else if (value->IsLibraryFunction())
        ss << "[library function]";
    else if (value->IsNativePtr())
        ss << "[native pointer]";
    else if (value->IsNil())
        ss << "[nil]";
    else
        assert(false);

    SET_RETVAL(ss.str());
}

void LibFunc::FileOpen(Object &env) {
    assert(env.IsValid());
    const auto &path = GetArgument(env, 0);
    const auto &mode = GetArgument(env, 1);

    if (!path->IsString()) THROW_WRONG_ARGUMENT;
    if (!mode->IsString()) THROW_WRONG_ARGUMENT;
    auto *fp = fopen(path->ToString().c_str(), mode->ToString().c_str());
    if (fp) {
        SET_RETVAL(Value(fp, NAT_POINTER_EXTERNAL_FILE));
    } else {
        SET_RETVAL(Value());
    }
}

void LibFunc::FileClose(Object &env) {
    assert(env.IsValid());
    const auto &fp = GetArgument(env, 0);

    if (!fp->IsNativePtr()) THROW_WRONG_ARGUMENT;
    if (fp->ToNativeTypeId() != NAT_POINTER_EXTERNAL_FILE) THROW_WRONG_ARGUMENT;

    if (fclose(static_cast<FILE *>(fp->ToNativePtr()))) {
        SET_RETVAL(false);
    } else {
        //FREE FP //TODO
        SET_RETVAL(true);
    }
}

void LibFunc::FileWrite(Object &env) {
    assert(env.IsValid());
    const auto &fp = GetArgument(env, 0);
    const auto &value = GetArgument(env, 1);

    if (!fp->IsNativePtr()) THROW_WRONG_ARGUMENT;
    if (fp->ToNativeTypeId() != NAT_POINTER_EXTERNAL_FILE) THROW_WRONG_ARGUMENT;
    if (!value->IsString()) THROW_WRONG_ARGUMENT;

    unsigned bytes = fwrite(value->ToString().c_str(), 1, value->ToString().size(), static_cast<FILE *>(fp->ToNativePtr()));
    fflush(static_cast<FILE *>(fp->ToNativePtr()));

    SET_RETVAL(static_cast<double>(bytes));
}

void LibFunc::FileRead(Object &env) {
    assert(env.IsValid());
    const auto &fp = GetArgument(env, 0);

    if (!fp->IsNativePtr()) THROW_WRONG_ARGUMENT;
    if (fp->ToNativeTypeId() != NAT_POINTER_EXTERNAL_FILE) THROW_WRONG_ARGUMENT;

    std::string input;
    char buffer[BUFFER_SIZE];
    size_t readBytes = 0;
    FILE *file = static_cast<FILE *>(fp->ToNativePtr());

    if (!file) Interpreter::GetInstance()->RuntimeError("File pointer is not valid");

    while ((readBytes = fread(buffer, 1, BUFFER_SIZE - 1, file)) != 0) {
        buffer[readBytes] = '\0';
        input += buffer;
    }

    SET_RETVAL(input);
}

void LibFunc::FileRemove(Object &env) {
    assert(env.IsValid());

    const Value *filename = GetArgument(env, 0);
    assert(filename);
    if (!filename->IsString()) STR_FUNC_WRONG_ARG("strlen", filename->GetTypeToString());

    bool result = std::remove(filename->ToString().c_str()) != 0;

    SET_RETVAL(result);
}

void LibFunc::Undef(Object &env) {
    assert(env.IsValid());

    SET_RETVAL(Value());
}

void LibFunc::Strlen(Object &env) {
    assert(env.IsValid());

    const Value *value = GetArgument(env, 0);
    if (!value->IsString()) STR_FUNC_WRONG_ARG("strlen", value->GetTypeToString());

    double result = value->ToString().size();
    SET_RETVAL(result);
}

void LibFunc::StrSearch(Object &env) {
    assert(env.IsValid());

    const Value *value1 = GetArgument(env, 0);
    const Value *value2 = GetArgument(env, 1);

    if (!value1->IsString()) STR_FUNC_WRONG_ARG("string_search (argument 1)", value1->GetTypeToString());
    if (!value2->IsString()) STR_FUNC_WRONG_ARG("string_search (argument 2)", value2->GetTypeToString());

    auto pos = value1->ToString().find(value2->ToString());

    if (pos != std::string::npos)
        env.Set(RETVAL_RESERVED_FIELD, (static_cast<double>(pos)));
    else
        SET_RETVAL(Value());
}

void LibFunc::StrSubstr(Object &env) {
    assert(env.IsValid());

    const Value *string = GetArgument(env, 0);
    const Value *start = GetArgument(env, 1);
    const Value *len = nullptr;

    if (env.ElementExists(2)) len = GetArgument(env, 2);

    if (!string->IsString()) STR_FUNC_WRONG_ARG("string_substr (argument 1)", string->GetTypeToString());
    if (!start->IsNumber()) NUM_FUNC_WRONG_ARG("string_search (argument 2)", start->GetTypeToString());
    if (len && !len->IsNumber()) NUM_FUNC_WRONG_ARG("string_search (argument 3)", start->GetTypeToString());

    if (start->ToNumber() >= string->ToString().size()) Interpreter::GetInstance()->RuntimeError("Library function \"string_search\" received starting position bigger than string's size");

    if (len)
        env.Set(RETVAL_RESERVED_FIELD, string->ToString().substr(start->ToNumber(), len->ToNumber()));
    else
        env.Set(RETVAL_RESERVED_FIELD, string->ToString().substr(start->ToNumber()));
}

void LibFunc::StrReplace(Object &env) {
    assert(env.IsValid());

    const Value *value1 = GetArgument(env, 0);
    const Value *value2 = GetArgument(env, 1);
    const Value *value3 = GetArgument(env, 2);

    if (!value1->IsString()) STR_FUNC_WRONG_ARG("string_replace (argument 1)", value1->GetTypeToString());
    if (!value2->IsString()) STR_FUNC_WRONG_ARG("string_replace (argument 2)", value2->GetTypeToString());
    if (!value3->IsString()) STR_FUNC_WRONG_ARG("string_replace (argument 3)", value3->GetTypeToString());

    std::string result = value1->ToString();
    std::string str1 = value2->ToString();
    std::string str2 = value3->ToString();

    result.replace(result.find(str1), str1.size(), str2);

    SET_RETVAL(result);
}

void LibFunc::StrTrim(Object &env) {
    assert(env.IsValid());

    const Value *value = GetArgument(env, 0);
    if (!value->IsString()) STR_FUNC_WRONG_ARG("string_trim", value->GetTypeToString());

    SET_RETVAL(Utilities::TrimString(value->ToString()));
}

void LibFunc::Popen(Object &env) {
    assert(env.IsValid());

    const Value *path = GetArgument(env, 0);
    const Value *mode = GetArgument(env, 1);

    if (!path->IsString()) STR_FUNC_WRONG_ARG("popen (argument 1)", path->GetTypeToString());
    if (!mode->IsString()) STR_FUNC_WRONG_ARG("popen (argument 2)", mode->GetTypeToString());

    FILE *process = popen(path->ToString().c_str(), mode->ToString().c_str());

    if (process)
        env.Set(RETVAL_RESERVED_FIELD, (Value(process, "EXTERNAL_FILE_PTR")));
    else
        SET_RETVAL(Value());
}

void LibFunc::Eval(Object &env) {
    assert(env.IsValid());

    const auto &evalString = GetArgument(env, 0);
    if (!evalString->IsString()) STR_FUNC_WRONG_ARG("eval", evalString->GetTypeToString());

    Object *parent = Interpreter::GetInstance()->GetLastASTStmt();
    assert(parent);

    const Value *lineNumber = (*parent)[LINE_NUMBER_RESERVED_FIELD];
    assert(lineNumber && lineNumber->IsNumber());

    SetLineOffset(lineNumber->ToNumber() - 1);

    Object * extraAST = ParseStringInput(evalString->ToString().c_str());
    assert(extraAST);

    ResetLineOffset();

    PostOrderTreeHost host({ new IncreaseRefCounterVisitor(), new SetParentTreeVisitor() });
    host.Accept(*extraAST);
    extraAST->Set(PARENT_RESERVED_FIELD, parent);

    PostOrderTreeHost validityHost(new ValidityVisitor());
    validityHost.Accept(*extraAST);

    const Value result = Interpreter::GetInstance()->Execute(*extraAST);
    SET_RETVAL(result);
}

void LibFunc::Include(Object &env) {
    static std::vector<std::string> included;
    assert(env.IsValid());
    const auto &path = GetArgument(env, 0);
    if (!path->IsString()) THROW_WRONG_ARGUMENT;

    //GUARD
    if (std::find(included.begin(), included.end(), path->ToString()) != included.end()) return;

    //FILE OPEN

    auto *filePtr = fopen(path->ToString().c_str(), "r");

    if (!filePtr) THROW_WRONG_ARGUMENT;

    included.push_back(path->ToString());
    //FILE READ

    fseek(filePtr, 0, SEEK_END);
    unsigned long size = ftell(filePtr);
    fseek(filePtr, 0, SEEK_SET);
    char *content = static_cast<char *>(malloc(sizeof(char) * size + 1));
    unsigned long readSize = fread(content, 1, size, filePtr);
    assert(readSize == size);
    content[size] = '\0';

    //EVAL

    auto extraAST = ParseStringInput(content);

    PostOrderTreeHost host({ new IncreaseRefCounterVisitor(), new SetParentTreeVisitor() });
    host.Accept(*extraAST);

    Object &parentNoConst = const_cast<Object &>(*(Interpreter::GetInstance()->GetLastASTStmt()));
    extraAST->Set(PARENT_RESERVED_FIELD, Value(&parentNoConst));

    PostOrderTreeHost validityHost(new ValidityVisitor());
    validityHost.Accept(*extraAST);

    Interpreter::GetInstance()->Execute(*extraAST);
}

#ifdef LIBCURL_SUPPORT
#include <curl/curl.h>

size_t Handler(void *buffer, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    std::string * target = static_cast<std::string *>(userp);
    target->append(static_cast<char *>(buffer), realsize);
    return realsize;
}

void LibFunc::Request(Object & env) {
    assert(env.IsValid());

    CURL * curl = nullptr;
    std::string result;

    const Value * post_fields = nullptr;
    const Value * url = GetArgument(env, 0);

    if (!url->IsString()) STR_FUNC_WRONG_ARG("request (argument 1)", url->GetTypeToString());

    if (env.GetNumericSize() > 1) post_fields = GetArgument(env, 1);
    if (post_fields && !post_fields->IsString()) STR_FUNC_WRONG_ARG("request (argument 2)", post_fields->GetTypeToString());

    curl = curl_easy_init();
    if (!curl) Interpreter::GetInstance()->RuntimeError("Could not initialize request context");

    curl_easy_setopt(curl, CURLOPT_URL, url->ToString().c_str());
    //curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, Handler);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);

    if (post_fields) {
        char * output = curl_easy_escape(curl, post_fields->ToString().c_str(), post_fields->ToString().size());

        curl_easy_setopt(curl, CURLOPT_POST, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, output);

        curl_free(output);
    }

    CURLcode res = curl_easy_perform(curl);

    if (res != CURLE_OK) {
        std::cerr << "Operation Failed: " << curl_easy_strerror(res) << std::endl;
        SET_RETVAL(Value());
    } else {
        SET_RETVAL(result);
    }

    curl_easy_cleanup(curl);
}
#else
void LibFunc::Request(Object & env) {
    assert(env.IsValid());
    Interpreter::GetInstance()->RuntimeError("Unsupported Library Function");
}
#endif