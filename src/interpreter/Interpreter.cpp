#include "Interpreter.h"
#include "CloneTreeVisitor.h"
#include "EvalMetaEscapesTreeVisitor.h"
#include "LibraryFunctions.h"
#include "PostOrderTreeHost.h"
#include "PreOrderTreeHost.h"
#include "SanityVisitor.h"
#include "SetParentTreeVisitor.h"
#include "TreeTags.h"
#include "UnparseVisitor.h"
#include "Utilities.h"
#include "parser.h"

#include "VisualizeVisitor.h"
#include "IncreaseRefCountVisitor.h"

#include <cassert>
#include <iostream>

Interpreter *Interpreter::instance = nullptr;
unsigned Interpreter::lineNumber = 0;

/****** Evaluators ******/

const Value Interpreter::EvalProgram(Object &node) {
    ASSERT_TYPE(AST_TAG_PROGRAM);
    return EVAL_CHILD();
}

const Value Interpreter::EvalStatements(Object &node) {
    ASSERT_TYPE(AST_TAG_STMTS);
    Value val = NIL_VAL;
    for (register unsigned i = 0; i < node.GetNumericSize(); ++i) {
        val = dispatcher.Eval(*node[i]->ToObject_NoConst());
    }
    return val;
}

const Value Interpreter::EvalStatement(Object &node) {
    ASSERT_TYPE(AST_TAG_STMT);
    lastASTStmt = &node;
    if (node.ElementExists(AST_TAG_CHILD)) return EVAL_CHILD();
    return NIL_VAL;
}

const Value Interpreter::EvalExpression(Object &node) {
    ASSERT_TYPE(AST_TAG_EXPR);
    return EVAL_CHILD();
}

const Value Interpreter::EvalAssign(Object &node) {
    ASSERT_TYPE(AST_TAG_ASSIGN);

    Symbol lvalue = EVAL_WRITE(AST_TAG_LVALUE);
    const Value rvalue = EVAL(AST_TAG_RVALUE);

    assert(lvalue.IsValid());
    assert(rvalue.IsValid());

    CHANGE_LINE();

    ValidateAssignment(lvalue, rvalue);

    /* Cases of assignment:
     * 1) f.$closure = [];
     * 2) t.x = nil;
     * 3) Anything else
     */
    if (lvalue.IsClosureChange())
        ChangeClosure(lvalue, rvalue);
    else if (lvalue.IsContextTable() && rvalue.IsNil())
        RemoveFromContext(lvalue, rvalue);
    else
        AssignToContext(lvalue, rvalue);

    return rvalue;
}

const Value Interpreter::EvalPlus(Object &node) {
    ASSERT_TYPE(AST_TAG_PLUS);
    return EvalMath(node, MathOp::Plus);
}

const Value Interpreter::EvalMinus(Object &node) {
    ASSERT_TYPE(AST_TAG_MINUS);
    return EvalMath(node, MathOp::Minus);
}

const Value Interpreter::EvalMul(Object &node) {
    ASSERT_TYPE(AST_TAG_MUL);
    return EvalMath(node, MathOp::Mul);
}

const Value Interpreter::EvalDiv(Object &node) {
    ASSERT_TYPE(AST_TAG_DIV);
    return EvalMath(node, MathOp::Div);
}

const Value Interpreter::EvalModulo(Object &node) {
    ASSERT_TYPE(AST_TAG_MODULO);
    return EvalMath(node, MathOp::Mod);
}

const Value Interpreter::EvalGreater(Object &node) {
    ASSERT_TYPE(AST_TAG_GREATER);
    return EvalMath(node, MathOp::Greater);
}

const Value Interpreter::EvalLess(Object &node) {
    ASSERT_TYPE(AST_TAG_LESS);
    return EvalMath(node, MathOp::Less);
}

const Value Interpreter::EvalGreaterEqual(Object &node) {
    ASSERT_TYPE(AST_TAG_GEQUAL);
    return EvalMath(node, MathOp::GreaterEqual);
}

const Value Interpreter::EvalLessEqual(Object &node) {
    ASSERT_TYPE(AST_TAG_LEQUAL);
    return EvalMath(node, MathOp::LessEqual);
}

const Value Interpreter::EvalEqual(Object &node) {
    ASSERT_TYPE(AST_TAG_EQUAL);

    auto op1 = EVAL(AST_TAG_FIRST_EXPR);
    auto op2 = EVAL(AST_TAG_SECOND_EXPR);

    CHANGE_LINE();

    return ValuesAreEqual(op1, op2);
}

const Value Interpreter::EvalNotEqual(Object &node) {
    ASSERT_TYPE(AST_TAG_NEQUAL);

    auto op1 = EVAL(AST_TAG_FIRST_EXPR);
    auto op2 = EVAL(AST_TAG_SECOND_EXPR);

    CHANGE_LINE();

    return !ValuesAreEqual(op1, op2);
}

const Value Interpreter::EvalAnd(Object &node) {
    ASSERT_TYPE(AST_TAG_AND);

    /* Short-circuit evaluation */
    auto op1 = EVAL(AST_TAG_FIRST_EXPR);
    if (!op1) return false;
    auto op2 = EVAL(AST_TAG_SECOND_EXPR);
    return static_cast<bool>(op2);
}

const Value Interpreter::EvalOr(Object &node) {
    ASSERT_TYPE(AST_TAG_OR);

    /* Short-circuit evaluation */
    auto op1 = EVAL(AST_TAG_FIRST_EXPR);
    if (op1) return true;
    auto op2 = EVAL(AST_TAG_SECOND_EXPR);
    return static_cast<bool>(op2);
}

const Value Interpreter::EvalTerm(Object &node) {
    ASSERT_TYPE(AST_TAG_TERM);
    return EVAL_CHILD();
}

const Value Interpreter::EvalUnaryMinus(Object &node) {
    ASSERT_TYPE(AST_TAG_UMINUS);

    const Value val = EVAL_CHILD();
    CHANGE_LINE();

    if (!val.IsNumber()) RuntimeError("Unary minus (-) cannot be applied to " + val.GetTypeToString());
    return -val.ToNumber();
}

const Value Interpreter::EvalNot(Object &node) {
    ASSERT_TYPE(AST_TAG_NOT);
    const Value val = EVAL_CHILD();
    return !static_cast<bool>(val);
}

const Value Interpreter::EvalPlusPlusBefore(Object &node) {
    ASSERT_TYPE(AST_TAG_BPLUSPLUS);
    return HandleAggregators(node, MathOp::Plus, true);
}

const Value Interpreter::EvalPlusPlusAfter(Object &node) {
    ASSERT_TYPE(AST_TAG_APLUSPLUS);
    return HandleAggregators(node, MathOp::Plus, false);
}

const Value Interpreter::EvalMinusMinusBefore(Object &node) {
    ASSERT_TYPE(AST_TAG_BMINUSMINUS);
    return HandleAggregators(node, MathOp::Minus, true);
}

const Value Interpreter::EvalMinusMinusAfter(Object &node) {
    ASSERT_TYPE(AST_TAG_AMINUSMINUS);
    return HandleAggregators(node, MathOp::Minus, false);
}

const Value Interpreter::EvalPrimary(Object &node) {
    ASSERT_TYPE(AST_TAG_PRIMARY);
    return EVAL_CHILD();
}

const Value Interpreter::EvalMetaParse(Object &node) {
    ASSERT_TYPE(AST_TAG_META_PARSE);
    CHANGE_LINE();

    const Value expr = EVAL_CHILD();
    if (!expr.IsString()) RuntimeError("Expected string in \"parse\" meta-operator. Found " + expr.GetTypeToString());

    assert(node.ElementExists(LINE_NUMBER_RESERVED_FIELD));
    assert(node[LINE_NUMBER_RESERVED_FIELD]->IsNumber());
    SetLineOffset(node[LINE_NUMBER_RESERVED_FIELD]->ToNumber() - 1);

    Object *ast = ParseStringInput(expr.ToString().c_str());
    ResetLineOffset();

    assert(ast);

    PostOrderTreeHost host{new IncreaseRefCounterVisitor()};
    host.Accept(*ast);

    return ast;
}

const Value Interpreter::EvalMetaUnparse(Object &node) {
    ASSERT_TYPE(AST_TAG_META_UNPARSE);
    CHANGE_LINE();

    const Value expr = EVAL_CHILD();
    if (!expr.IsObject()) RuntimeError("Expected object in \"unparse\" meta-operator. Found " + expr.GetTypeToString());

    if (!IsValidAST(*expr.ToObject()))
        RuntimeError("Ill-formed Abstract Syntax Tree");

    UnparseVisitor *visitor = new UnparseVisitor();
    PostOrderTreeHost host{visitor};
    host.Accept(*expr.ToObject());

    std::string result = visitor->GetResult();

    return Utilities::TrimString(result);
}

const Value Interpreter::EvalMetaInline(Object &node) {
    ASSERT_TYPE(AST_TAG_META_INLINE);
    CHANGE_LINE();

    const Value &value = MetaCheckAndClone(node);
    assert(value.IsObject());

    return dispatcher.Eval(*(value.ToObject_NoConst()));
}

const Value Interpreter::EvalMetaEscape(const Object &node) {
    ASSERT_TYPE(AST_TAG_META_ESCAPE);
    CHANGE_LINE();

    const Value &value = MetaCheckAndClone(node);
    assert(value.IsObject());

    return value;
}

const Value Interpreter::EvalMetaQuasiQuotes(Object &node) {
    ASSERT_TYPE(AST_TAG_META_QUASI_QUOTES);
    CHANGE_LINE();

    Object *ast = node[AST_TAG_CHILD]->ToObject_NoConst();

    CloneTreeVisitor *cloneTreeVisitor = new CloneTreeVisitor();
    EvalMetaEscapesTreeVisitor *evalMetaEscapesTreeVisitor = new EvalMetaEscapesTreeVisitor();
    evalMetaEscapesTreeVisitor->SetEvalMetaEscape([this](const Object &n) { EvalMetaEscape(n); });

    PostOrderTreeHost cloneTreeHost { cloneTreeVisitor };
    PostOrderTreeHost evalMetaEscapesTreeHost { evalMetaEscapesTreeVisitor };

    cloneTreeHost.Accept(*ast);

    Object *cloneAst = cloneTreeVisitor->GetResult();
    evalMetaEscapesTreeHost.Accept(*cloneAst);

    return Value(cloneAst);
}

const Value Interpreter::EvalLValue(Object &node) {
    ASSERT_TYPE(AST_TAG_LVALUE);
    return EVAL_CHILD();
}

const Value Interpreter::EvalId(Object &node) {
    ASSERT_TYPE(AST_TAG_ID);

    /* Lookup all scopes starting from the current one and moving backwards. If
     * the given symbol is not found in any scope then add in the current one.
     * If the symbol already exists in a scope, we get its value */

    std::string symbol = node[AST_TAG_ID]->ToString();

#define NO_LIBFUNC_LOOKUP
#ifdef NO_LIBFUNC_LOOKUP
    if (IsLibFunc(symbol)) {
        assert(globalScope->ElementExists(symbol));
        return (*(*globalScope)[symbol]);
    }
#endif

    Object *scope = FindScope(symbol);
    if (!scope) {
        currentScope->Set(symbol, Value());
        scope = currentScope;
    }

    assert(scope && scope->ElementExists(symbol));
    return (*(*scope)[symbol]);
}

const Value Interpreter::EvalLocal(Object &node) {
    ASSERT_TYPE(AST_TAG_LOCAL_ID);
    CHANGE_LINE();

    /* Lookup the current scope. If the symbol is found then get its value. If
     * nothing is found, we have to create a new symbol in the current scope.
     * Before doing so we must check for a collision with a library function. */

    std::string symbol = node[AST_TAG_ID]->ToString();

    const Value *var = LookupCurrentScope(symbol);
    if (var) return *var;

    if (IsLibFunc(symbol)) RuntimeError("Local variable \"" + symbol + "\" shadows library function");

    currentScope->Set(symbol, Value());
    return Value();
}

const Value Interpreter::EvalDoubleColon(Object &node) {
    ASSERT_TYPE(AST_TAG_DOUBLECOLON_ID);
    CHANGE_LINE();

    /* Lookup global scope. If a symbol is found then get its value. If none is
     * found: Runtime error */

    std::string symbol = node[AST_TAG_ID]->ToString();
    const Value *val = LookupGlobalScope(symbol);
    if (!val) RuntimeError("Global symbol \"" + symbol + "\" does not exist (Undefined Symbol)");

    return *val;
}

const Value Interpreter::EvalDollar(Object &node) {
    assert(false);
    return NIL_VAL;
}

const Value Interpreter::EvalMember(Object &node) {
    ASSERT_TYPE(AST_TAG_MEMBER);
    return EVAL_CHILD();
}

const Value Interpreter::EvalDot(Object &node) {
    ASSERT_TYPE(AST_TAG_DOT);
    const Value lvalue = EVAL(AST_TAG_LVALUE);
    const Value index = GetIdName(*node[AST_TAG_ID]->ToObject());
    return TableGetElem(lvalue, index);
}

const Value Interpreter::EvalBracket(Object &node) {
    ASSERT_TYPE(AST_TAG_BRACKET);
    const Value lvalue = EVAL(AST_TAG_LVALUE);
    const Value index = EVAL(AST_TAG_EXPR);
    return TableGetElem(lvalue, index);
}

const Value Interpreter::EvalDollarEnv(Object &node) {
    ASSERT_TYPE(AST_TAG_DOLLAR_ENV);
    return Value(currentScope);
}

const Value Interpreter::EvalDollarLambda(Object &node) {
    ASSERT_TYPE(AST_TAG_DOLLAR_LAMBDA);

    assert(!callStack.IsEmpty());
    const Value top = callStack.Top();
    assert(top.IsProgramFunction());
    return top;
}

const Value Interpreter::EvalCall(Object &node) {
    ASSERT_TYPE(AST_TAG_CALL);
    CHANGE_LINE();
    Value callable, lvalue;

    if (node.ElementExists(AST_TAG_LVALUE)) {
        lvalue = EVAL(AST_TAG_LVALUE);    //Evaluate lvalue
        const Object *idNode = node[AST_TAG_FUNCTION]->ToObject();
        std::string id = (*idNode)[AST_TAG_ID]->ToString();
        callable = TableGetElem(lvalue, id);
        assert(lvalue.IsObject());
        lvalue.ToObject_NoConst()->IncreaseRefCounter();    //moved from CallProgramFunction
    } else {
        Value functionVal = EVAL(AST_TAG_FUNCTION);
        if (functionVal.IsObject()) {
            const Value *element = (*functionVal.ToObject())["()"];
            if (!element)
                RuntimeError("Cannot call an object if it is not a functor");
            callable = *element;
        } else {
            callable = functionVal;
        }
    }
    if (!callable.IsLibraryFunction() && !callable.IsProgramFunction())
        RuntimeError("Cannot call something that is not a function. Found " + callable.GetTypeToString());

    Object *arguments = node[AST_TAG_ARGUMENTS]->ToObject_NoConst();
    Object *evalArguments = dispatcher.Eval(*arguments).ToObject_NoConst();

    Value result;
    if (callable.IsProgramFunction()) {
        Object *functionClosure = (callable.ToProgramFunctionClosure_NoConst());
        const Object *functionAst = callable.ToProgramFunctionAST();
        const Object *formals = (*functionAst)[AST_TAG_FUNCTION_FORMALS]->ToObject();
        Object *body = (*functionAst)[AST_TAG_STMT]->ToObject_NoConst();
        callStack.Push(callable);
        result = CallProgramFunction(functionClosure, arguments, formals, body, evalArguments, lvalue);
        callStack.Pop();
    } else if (callable.IsLibraryFunction()) {
        std::string functionId = callable.ToLibraryFunctionId();
        LibraryFunc functionLib = callable.ToLibraryFunction();
        result = CallLibraryFunction(functionId, functionLib, evalArguments, lvalue);
    } else {
        assert(false);
    }

    //evalArguments is a temporary implementation Object
    //So we manually delete this memory
    evalArguments->Clear();
    return result;
}

//Evaluates ArgumentList with positional information
const Value Interpreter::EvalArgumentList(Object &node) {
    ASSERT_TYPE(AST_TAG_ARGLIST);

    Object *table = new Object();
    unsigned offset = 0;

    for (register unsigned i = 0; i < node.GetNumericSize(); ++i) {
        Object *argument = node[i]->ToObject_NoConst();
        const Value v = dispatcher.Eval(*argument);

        if (IsRuntimeEmpty(v)){
            offset++;
            continue;
        }

        // We must increase references WHILE evaluating arguments at all times
        // This way we immediately transfer return value ownership in such cases: g(f(), f()) where f returns object
        // If not, future writes to retValRegister will steal ownership and prematurely free useful memory, resulting in segmentation
        // In ProgramFunction cases, evalArgumentList object references are decreased in BlockExit()
        // In LibraryFunction cases, evalArgumentList object references are decreased manually in CallLibraryFunction()
        // Also check lvalue manual reference increase in EvalCall
        if (v.IsObject())
            v.ToObject_NoConst()->IncreaseRefCounter();
        else if (v.IsProgramFunction())
            v.ToProgramFunctionClosure_NoConst()->IncreaseRefCounter();

        table->Set(i-offset, v);
    }

    return table;
}

const Value Interpreter::EvalNamedArgument(Object &node) {
    ASSERT_TYPE(AST_TAG_NAMED);
    // EVAL(AST_TAG_NAMED_KEY);
    return EVAL(AST_TAG_NAMED_VALUE);
}

const Value Interpreter::EvalExpressionList(Object &node) {
    ASSERT_TYPE(AST_TAG_ELIST);

    Object *table = new Object();
    unsigned offset = 0;

    for (register unsigned i = 0; i < node.GetNumericSize(); ++i) {
        const Value v = dispatcher.Eval(*node[i]->ToObject_NoConst());
        if (IsRuntimeEmpty(v)){
            offset++;
            continue;
        }

        if (v.IsObject())
            v.ToObject_NoConst()->IncreaseRefCounter();
        else if (v.IsProgramFunction())
            v.ToProgramFunctionClosure_NoConst()->IncreaseRefCounter();

        table->Set(i - offset, v);
    }

    return table;
}

const Value Interpreter::EvalObjectDef(Object &node) {
    ASSERT_TYPE(AST_TAG_OBJECT_DEF);
    return EVAL(AST_TAG_CHILD);
}

const Value Interpreter::EvalIndexed(Object &node) {
    ASSERT_TYPE(AST_TAG_INDEXED);

    Object *table = new Object();
    for (register unsigned i = 0; i < node.GetNumericSize(); ++i) {
        const Value v = dispatcher.Eval(*node[i]->ToObject_NoConst());
        assert(v.IsObject());

        Object *pair = v.ToObject_NoConst();
        pair->Visit([table](const Value &key, const Value &val) {
            if (key.IsString())
                table->Set(key.ToString(), val);
            else if (key.IsNumber())
                table->Set(key.ToNumber(), val);
            else
                assert(false);

            if (val.IsObject())
                val.ToObject_NoConst()->IncreaseRefCounter();
            else if (val.IsProgramFunction())
                val.ToProgramFunctionClosure_NoConst()->IncreaseRefCounter();
        });

        pair->Clear();
        delete pair;
    }

    return table;
}

const Value Interpreter::EvalIndexedElem(Object &node) {
    ASSERT_TYPE(AST_TAG_INDEXED_ELEM);

    Object *pair = new Object();

    auto key = EVAL(AST_TAG_OBJECT_KEY);
    auto value = EVAL(AST_TAG_OBJECT_VALUE);

    CHANGE_LINE();

    if (key.IsString()) {
        ValidateAssignment(Symbol(pair, key.ToString()), value);
        pair->Set(key.ToString(), value);
    } else if (key.IsNumber()) {
        ValidateAssignment(Symbol(pair, key.ToNumber()), value);
        pair->Set(key.ToNumber(), value);
    } else
        RuntimeError("Keys of objects can only be strings or numbers. Found " + key.GetTypeToString());

    return pair;
}

const Value Interpreter::EvalBlock(Object &node) {
    ASSERT_TYPE(AST_TAG_BLOCK);
    assert(currentScope == scopeStack.front());

    if (!inFunctionScope) BlockEnter();
    inFunctionScope = false;
    try {
        EVAL_CHILD();
    } catch (const BreakException &e) {
        BlockExit();
        throw e;
    } catch (const ContinueException &e) {
        BlockExit();
        throw e;
    } catch (const ReturnException &e) {
        BlockExit();
        throw e;
    }
    BlockExit();

    return NIL_VAL;
}

const Value Interpreter::EvalFunctionDef(Object &node) {
    ASSERT_TYPE(AST_TAG_FUNCTION_DEF);
    assert(currentScope == scopeStack.front());

    const Object *child = node[AST_TAG_FUNCTION_ID]->ToObject();
    assert(child);
    std::string name = (*child)[AST_TAG_ID]->ToString();

    CHANGE_LINE();

    if (IsLibFunc(name)) RuntimeError("Cannot define function \"" + name + "\". It shadows the library function");
    if (LookupCurrentScope(name)) RuntimeError("Cannot define function \"" + name + "\". Symbol name already exists");

    Object *functionScope = currentScope;
    assert(functionScope);
    currentScope->Set(name, Value(&node, currentScope));
    currentScope->IncreaseRefCounter();

    currentScope = PushSlice();

    return Value(&node, functionScope);
}

const Value Interpreter::EvalConst(Object &node) {
    ASSERT_TYPE(AST_TAG_CONST);
    return EVAL_CHILD();
}

const Value Interpreter::EvalNumber(Object &node) {
    ASSERT_TYPE(AST_TAG_NUMBER);
    return *node[AST_TAG_VALUE];
}

const Value Interpreter::EvalString(Object &node) {
    ASSERT_TYPE(AST_TAG_STRING);
    return *node[AST_TAG_VALUE];
}

const Value Interpreter::EvalNil(Object &node) {
    ASSERT_TYPE(AST_TAG_NIL);
    return NIL_VAL;
}

const Value Interpreter::EvalTrue(Object &node) {
    ASSERT_TYPE(AST_TAG_TRUE);
    return Value(true);
}

const Value Interpreter::EvalFalse(Object &node) {
    ASSERT_TYPE(AST_TAG_FALSE);
    return Value(false);
}

const Value Interpreter::EvalIdList(Object &node) {
    ASSERT_TYPE(AST_TAG_ID_LIST);
    assert(false);
    return NIL_VAL;
}

const Value Interpreter::EvalFormal(Object &node) {
    CHANGE_LINE();

    std::string formalName = node[AST_TAG_ID]->ToString();

    if (IsLibFunc(formalName)) RuntimeError("Formal argument \"" + formalName + "\" shadows library function");
    if (LookupCurrentScope(formalName)) RuntimeError("Formal argument \"" + formalName + "\" already defined as a formal");

    const Value val = Value();
    currentScope->Set(formalName, val);
    return val;
}

const Value Interpreter::EvalIf(Object &node) {
    ASSERT_TYPE(AST_TAG_IF);

    if (EVAL(AST_TAG_CONDITION))
        EVAL(AST_TAG_STMT);
    else if (node.ElementExists(AST_TAG_ELSE_STMT))
        EVAL(AST_TAG_ELSE_STMT);

    return NIL_VAL;
}

const Value Interpreter::EvalWhile(Object &node) {
    ASSERT_TYPE(AST_TAG_WHILE);

    while (EVAL(AST_TAG_CONDITION)) {
        try {
            EVAL(AST_TAG_STMT);
        } catch (const BreakException &e) {
            break;
        } catch (const ContinueException &e) {
            continue;
        }
    }

    return NIL_VAL;
}

void HandleElist(Value &elist2) {
    if (elist2.IsObject()) {
        elist2.ToObject_NoConst()->Clear();
        delete elist2.ToObject_NoConst();
    }
}

const Value Interpreter::EvalFor(Object &node) {
    ASSERT_TYPE(AST_TAG_FOR);

    Value elist1, elist2;

    for (elist1 = EVAL(AST_TAG_FOR_PRE_ELIST); EVAL(AST_TAG_CONDITION); HandleElist(elist2), elist2 = EVAL(AST_TAG_FOR_POST_ELIST)) {
        try {
            EVAL(AST_TAG_STMT);
        } catch (const BreakException &e) {
            break;
        } catch (const ContinueException &e) {
            continue;
        } catch (const ReturnException &e) {
            CleanupForLoop(elist1, elist2);
            throw e;
        }
    }

    CleanupForLoop(elist1, elist2);

    return NIL_VAL;
}

const Value Interpreter::EvalReturn(Object &node) {
    ASSERT_TYPE(AST_TAG_RETURN);
    Value result;    // initialized to undef, in case function never returns a value
    if (node.ElementExists(AST_TAG_CHILD)) result = EVAL_CHILD();
    SetRetvalRegister(result);
    throw ReturnException();
}

const Value Interpreter::EvalBreak(Object &node) {
    ASSERT_TYPE(AST_TAG_BREAK);
    throw BreakException();
}

const Value Interpreter::EvalContinue(Object &node) {
    ASSERT_TYPE(AST_TAG_CONTINUE);
    throw ContinueException();
}

const Value Interpreter::EvalRuntimeEmpty(Object &node) {
    ASSERT_TYPE(AST_TAG_RUNTIME_EMPTY);
    return Value(&node);
}

Interpreter *Interpreter::GetInstance(void) {
    if (!instance) instance = new Interpreter();
    return instance;
}

Object *Interpreter::GetLastASTStmt() {
    return lastASTStmt;
}

void Interpreter::SetAnonymousFuncOffset(unsigned num) {
    anonymousFuncOffset = num;
}