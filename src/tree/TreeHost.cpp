#include "TreeHost.h"

#include "Object.h"
#include "TreeTags.h"

#include <cassert>

TreeHost::TreeHost(){
    InstallAllAcceptors();
}

TreeHost::TreeHost(TreeVisitor *_visitor) {
    assert(_visitor);
    visitors.push_back(_visitor);
    InstallAllAcceptors();
}

TreeHost::TreeHost(const std::vector<TreeVisitor*>& _visitors) : visitors(_visitors) {
    InstallAllAcceptors();
}

TreeHost::~TreeHost() {
    for (TreeVisitor* visitor : visitors)
        delete visitor;
    acceptors.clear();
    visitors.clear();
}

void TreeHost::Accept(const Object &node) {
    assert(node.ElementExists(AST_TAG_TYPE_KEY)); // TODO: temp sol -> This should be handled differently
    acceptors[node[AST_TAG_TYPE_KEY]->ToString()](node);
}

void TreeHost::InstallAcceptor(const std::string &tag, const Acceptor &f) {
    assert(f);
    acceptors[tag] = f;
}

void TreeHost::AddVisitor(TreeVisitor* visitor){
    visitors.push_back(visitor);
}

std::vector<std::string> TreeHost::GetInstalledTags(void) const {
    std::vector<std::string> tags;

    for(auto i = acceptors.begin(); i != acceptors.end(); ++i) tags.push_back(i->first);

    return tags;
}

#define LAMBDA(f) [this](const Object &node) { f(node); }

void TreeHost::InstallAllAcceptors(void) {
    InstallAcceptor(AST_TAG_PROGRAM, LAMBDA(AcceptProgram));
    InstallAcceptor(AST_TAG_STMTS, LAMBDA(AcceptStatements));
    InstallAcceptor(AST_TAG_STMT, LAMBDA(AcceptStatement));
    InstallAcceptor(AST_TAG_EXPR, LAMBDA(AcceptExpression));
    InstallAcceptor(AST_TAG_ASSIGN, LAMBDA(AcceptAssign));
    InstallAcceptor(AST_TAG_PLUS, LAMBDA(AcceptPlus));
    InstallAcceptor(AST_TAG_MINUS, LAMBDA(AcceptMinus));
    InstallAcceptor(AST_TAG_MUL, LAMBDA(AcceptMul));
    InstallAcceptor(AST_TAG_DIV, LAMBDA(AcceptDiv));
    InstallAcceptor(AST_TAG_MODULO, LAMBDA(AcceptModulo));
    InstallAcceptor(AST_TAG_GREATER, LAMBDA(AcceptGreater));
    InstallAcceptor(AST_TAG_LESS, LAMBDA(AcceptLess));
    InstallAcceptor(AST_TAG_GEQUAL, LAMBDA(AcceptGreaterEqual));
    InstallAcceptor(AST_TAG_LEQUAL, LAMBDA(AcceptLessEqual));
    InstallAcceptor(AST_TAG_EQUAL, LAMBDA(AcceptEqual));
    InstallAcceptor(AST_TAG_NEQUAL, LAMBDA(AcceptNotEqual));
    InstallAcceptor(AST_TAG_AND, LAMBDA(AcceptAnd));
    InstallAcceptor(AST_TAG_OR, LAMBDA(AcceptOr));
    InstallAcceptor(AST_TAG_TERM, LAMBDA(AcceptTerm));
    InstallAcceptor(AST_TAG_UMINUS, LAMBDA(AcceptUnaryMinus));
    InstallAcceptor(AST_TAG_NOT, LAMBDA(AcceptNot));
    InstallAcceptor(AST_TAG_BPLUSPLUS, LAMBDA(AcceptPlusPlusBefore));
    InstallAcceptor(AST_TAG_APLUSPLUS, LAMBDA(AcceptPlusPlusAfter));
    InstallAcceptor(AST_TAG_BMINUSMINUS, LAMBDA(AcceptMinusMinusBefore));
    InstallAcceptor(AST_TAG_AMINUSMINUS, LAMBDA(AcceptMinusMinusAfter));
    InstallAcceptor(AST_TAG_PRIMARY, LAMBDA(AcceptPrimary));
    InstallAcceptor(AST_TAG_META_PARSE, LAMBDA(AcceptMetaParse));
    InstallAcceptor(AST_TAG_META_UNPARSE, LAMBDA(AcceptMetaUnparse));
    InstallAcceptor(AST_TAG_META_INLINE, LAMBDA(AcceptMetaInline));
    InstallAcceptor(AST_TAG_META_ESCAPE, LAMBDA(AcceptMetaEscape));
    InstallAcceptor(AST_TAG_META_QUASI_QUOTES, LAMBDA(AcceptMetaQuasiQuotes));
    InstallAcceptor(AST_TAG_RUNTIME_EMPTY, LAMBDA(AcceptRuntimeEmpty));
    InstallAcceptor(AST_TAG_LVALUE, LAMBDA(AcceptLValue));
    InstallAcceptor(AST_TAG_ID, LAMBDA(AcceptId));
    InstallAcceptor(AST_TAG_LOCAL_ID, LAMBDA(AcceptLocal));
    InstallAcceptor(AST_TAG_DOUBLECOLON_ID, LAMBDA(AcceptDoubleColon));
    InstallAcceptor(AST_TAG_DOLLAR_ID, LAMBDA(AcceptDollar));
    InstallAcceptor(AST_TAG_DOLLAR_LAMBDA, LAMBDA(AcceptDollarEnv));
    InstallAcceptor(AST_TAG_DOLLAR_LAMBDA, LAMBDA(AcceptDollarLambda));
    InstallAcceptor(AST_TAG_DOLLAR_ENV, LAMBDA(AcceptDollarEnv));
    InstallAcceptor(AST_TAG_MEMBER, LAMBDA(AcceptMember));
    InstallAcceptor(AST_TAG_DOT, LAMBDA(AcceptDot));
    InstallAcceptor(AST_TAG_BRACKET, LAMBDA(AcceptBracket));
    InstallAcceptor(AST_TAG_CALL, LAMBDA(AcceptCall));
    InstallAcceptor(AST_TAG_ARGLIST, LAMBDA(AcceptArgumentList));
    InstallAcceptor(AST_TAG_NAMED, LAMBDA(AcceptNamedArgument));
    InstallAcceptor(AST_TAG_ELIST, LAMBDA(AcceptExpressionList));
    InstallAcceptor(AST_TAG_OBJECT_DEF, LAMBDA(AcceptObjectDef));
    InstallAcceptor(AST_TAG_INDEXED, LAMBDA(AcceptIndexed));
    InstallAcceptor(AST_TAG_INDEXED_ELEM, LAMBDA(AcceptIndexedElem));
    InstallAcceptor(AST_TAG_BLOCK, LAMBDA(AcceptBlock));
    InstallAcceptor(AST_TAG_FUNCTION_DEF, LAMBDA(AcceptFunctionDef));
    InstallAcceptor(AST_TAG_CONST, LAMBDA(AcceptConst));
    InstallAcceptor(AST_TAG_NUMBER, LAMBDA(AcceptNumber));
    InstallAcceptor(AST_TAG_STRING, LAMBDA(AcceptString));
    InstallAcceptor(AST_TAG_NIL, LAMBDA(AcceptNil));
    InstallAcceptor(AST_TAG_TRUE, LAMBDA(AcceptTrue));
    InstallAcceptor(AST_TAG_FALSE, LAMBDA(AcceptFalse));
    InstallAcceptor(AST_TAG_ID_LIST, LAMBDA(AcceptIdList));
    InstallAcceptor(AST_TAG_FORMAL, LAMBDA(AcceptFormal));
    InstallAcceptor(AST_TAG_IF, LAMBDA(AcceptIf));
    InstallAcceptor(AST_TAG_WHILE, LAMBDA(AcceptWhile));
    InstallAcceptor(AST_TAG_FOR, LAMBDA(AcceptFor));
    InstallAcceptor(AST_TAG_RETURN, LAMBDA(AcceptReturn));
    InstallAcceptor(AST_TAG_BREAK, LAMBDA(AcceptBreak));
    InstallAcceptor(AST_TAG_CONTINUE, LAMBDA(AcceptContinue));
}

void TreeHost::AcceptProgram(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_PROGRAM);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptStatements(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_STMTS);

    for (unsigned i = 0; i < node.GetNumericSize(); ++i)
        Accept(*node[i]->ToObject());
}

void TreeHost::AcceptStatement(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_STMT);

    if (node.ElementExists(AST_TAG_CHILD))
        Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptExpression(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_EXPR);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptAssign(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ASSIGN);
    assert(node.ElementExists(AST_TAG_LVALUE));
    assert(node.ElementExists(AST_TAG_RVALUE));

    Accept(*node[AST_TAG_LVALUE]->ToObject());
    Accept(*node[AST_TAG_RVALUE]->ToObject());
}

void TreeHost::AcceptPlus(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_PLUS);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptMinus(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_MINUS);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptMul(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_MUL);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptDiv(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_DIV);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptModulo(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_MODULO);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptGreater(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_GREATER);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptLess(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_LESS);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptGreaterEqual(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_GEQUAL);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptLessEqual(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_LEQUAL);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptEqual(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_EQUAL);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptNotEqual(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_NEQUAL);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptAnd(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_AND);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptOr(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_OR);
    assert(node.ElementExists(AST_TAG_FIRST_EXPR));
    assert(node.ElementExists(AST_TAG_SECOND_EXPR));

    Accept(*node[AST_TAG_FIRST_EXPR]->ToObject());
    Accept(*node[AST_TAG_SECOND_EXPR]->ToObject());
}

void TreeHost::AcceptTerm(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_TERM);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptUnaryMinus(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_UMINUS);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptNot(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_NOT);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptPlusPlusBefore(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_BPLUSPLUS);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptPlusPlusAfter(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_APLUSPLUS);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptMinusMinusBefore(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_BMINUSMINUS);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptMinusMinusAfter(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_AMINUSMINUS);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptPrimary(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_PRIMARY);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptMetaParse(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_META_PARSE);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptMetaUnparse(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_META_UNPARSE);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptMetaInline(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_META_INLINE);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptMetaEscape(const Object &node){
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_META_ESCAPE);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptMetaQuasiQuotes(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_META_QUASI_QUOTES);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptRuntimeEmpty(const Object &node){
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_RUNTIME_EMPTY);
}

void TreeHost::AcceptLValue(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_LVALUE);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptId(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ID);
    assert(node.ElementExists(AST_TAG_ID));
}

void TreeHost::AcceptLocal(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_LOCAL_ID);
    assert(node.ElementExists(AST_TAG_ID));
}

void TreeHost::AcceptDoubleColon(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_DOUBLECOLON_ID);
    assert(node.ElementExists(AST_TAG_ID));
}

void TreeHost::AcceptDollar(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_DOLLAR_ID);
    assert(node.ElementExists(AST_TAG_ID));
}

void TreeHost::AcceptDollarEnv(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_DOLLAR_ENV);
    assert(node.ElementExists(AST_TAG_ID));
}

void TreeHost::AcceptDollarLambda(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_DOLLAR_LAMBDA);
    assert(node.ElementExists(AST_TAG_ID));
}

void TreeHost::AcceptMember(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_MEMBER);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptDot(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_DOT);
    assert(node.ElementExists(AST_TAG_LVALUE));
    assert(node.ElementExists(AST_TAG_ID));

    Accept(*node[AST_TAG_LVALUE]->ToObject());
    Accept(*node[AST_TAG_ID]->ToObject());
}

void TreeHost::AcceptBracket(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_BRACKET);
    assert(node.ElementExists(AST_TAG_LVALUE));
    assert(node.ElementExists(AST_TAG_EXPR));

    Accept(*node[AST_TAG_LVALUE]->ToObject());
    Accept(*node[AST_TAG_EXPR]->ToObject());
}

void TreeHost::AcceptCall(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_CALL);
    assert(node.ElementExists(AST_TAG_FUNCTION));
    assert(node.ElementExists(AST_TAG_ARGUMENTS));

    if (node.ElementExists(AST_TAG_LVALUE))
        Accept(*node[AST_TAG_LVALUE]->ToObject());

    Accept(*node[AST_TAG_FUNCTION]->ToObject());
    Accept(*node[AST_TAG_ARGUMENTS]->ToObject());
}

void TreeHost::AcceptArgumentList(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ARGLIST);

    for (unsigned i = 0; i < node.GetNumericSize(); ++i)
        Accept(*node[i]->ToObject());
}

void TreeHost::AcceptNamedArgument(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_NAMED);

    Accept(*node[AST_TAG_NAMED_KEY]->ToObject());    //Optional
    Accept(*node[AST_TAG_NAMED_VALUE]->ToObject());
}

void TreeHost::AcceptExpressionList(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ELIST);

    for (unsigned i = 0; i < node.GetNumericSize(); ++i)
        Accept(*node[i]->ToObject());
}

void TreeHost::AcceptObjectDef(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_OBJECT_DEF);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptIndexed(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_INDEXED);

    for (unsigned i = 0; i < node.GetNumericSize(); ++i)
        Accept(*node[i]->ToObject());
}

void TreeHost::AcceptIndexedElem(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_INDEXED_ELEM);
    assert(node.ElementExists(AST_TAG_OBJECT_KEY));
    assert(node.ElementExists(AST_TAG_OBJECT_VALUE));

    Accept(*node[AST_TAG_OBJECT_KEY]->ToObject());
    Accept(*node[AST_TAG_OBJECT_VALUE]->ToObject());
}

void TreeHost::AcceptBlock(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_BLOCK);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptFunctionDef(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FUNCTION_DEF);
    assert(node.ElementExists(AST_TAG_FUNCTION_ID));
    assert(node.ElementExists(AST_TAG_FUNCTION_FORMALS));
    assert(node.ElementExists(AST_TAG_STMT));

    Accept(*node[AST_TAG_FUNCTION_ID]->ToObject());    //This is optional
    Accept(*node[AST_TAG_FUNCTION_FORMALS]->ToObject());
    Accept(*node[AST_TAG_STMT]->ToObject());
}

void TreeHost::AcceptConst(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_CONST);
    assert(node.ElementExists(AST_TAG_CHILD));

    Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptNumber(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_NUMBER);
    assert(node.ElementExists(AST_TAG_VALUE));
}

void TreeHost::AcceptString(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_STRING);
    assert(node.ElementExists(AST_TAG_VALUE));
}

void TreeHost::AcceptNil(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_NIL);
    assert(node.ElementExists(AST_TAG_VALUE));
}

void TreeHost::AcceptTrue(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_TRUE);
    assert(node.ElementExists(AST_TAG_VALUE));
}

void TreeHost::AcceptFalse(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FALSE);
    assert(node.ElementExists(AST_TAG_VALUE));
}

void TreeHost::AcceptIdList(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ID_LIST);

    for (unsigned i = 0; i < node.GetNumericSize(); ++i)
        Accept(*node[i]->ToObject());
}

void TreeHost::AcceptFormal(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FORMAL);
    assert(node.ElementExists(AST_TAG_ID));
}

void TreeHost::AcceptIf(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_IF);
    assert(node.ElementExists(AST_TAG_CONDITION));
    assert(node.ElementExists(AST_TAG_STMT));

    Accept(*node[AST_TAG_CONDITION]->ToObject());
    Accept(*node[AST_TAG_STMT]->ToObject());
    if (node.ElementExists(AST_TAG_ELSE_STMT))
        Accept(*node[AST_TAG_ELSE_STMT]->ToObject());
}

void TreeHost::AcceptWhile(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_WHILE);
    assert(node.ElementExists(AST_TAG_CONDITION));
    assert(node.ElementExists(AST_TAG_STMT));

    Accept(*node[AST_TAG_CONDITION]->ToObject());
    Accept(*node[AST_TAG_STMT]->ToObject());
}

void TreeHost::AcceptFor(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FOR);
    assert(node.ElementExists(AST_TAG_FOR_PRE_ELIST));
    assert(node.ElementExists(AST_TAG_CONDITION));
    assert(node.ElementExists(AST_TAG_FOR_POST_ELIST));
    assert(node.ElementExists(AST_TAG_STMT));

    Accept(*node[AST_TAG_FOR_PRE_ELIST]->ToObject());
    Accept(*node[AST_TAG_CONDITION]->ToObject());
    Accept(*node[AST_TAG_FOR_POST_ELIST]->ToObject());
    Accept(*node[AST_TAG_STMT]->ToObject());
}

void TreeHost::AcceptReturn(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_RETURN);

    if (node.ElementExists(AST_TAG_CHILD))
        Accept(*node[AST_TAG_CHILD]->ToObject());
}

void TreeHost::AcceptBreak(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_BREAK);
}

void TreeHost::AcceptContinue(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_CONTINUE);
}
