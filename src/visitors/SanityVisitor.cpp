#include "SanityVisitor.h"
#include "TreeTags.h"
#include "Utilities.h"

#include <string>

SanityException::SanityException(const std::string &m) : msg(m) {}

const char *SanityException::what() const throw() {
    return Utilities::TrimString(msg).c_str();
}

#define CHECK_TYPE(x)                            \
    if (!node.IsValid() &&                       \
        !node.ElementExists(AST_TAG_TYPE_KEY) && \
        node[AST_TAG_TYPE_KEY]->ToString() != x) { throw SanityException("Invalid type. Expected " x); }

#define CHECK_CHILDREN(...) CheckChildren(node, __VA_ARGS__)

#define HAS_ELEMENTS(...) HasElement(node, __VA_ARGS__)

#define CHILDREN_IS_TYPE_OF(x, ...) ChildrenType(*(node[x]->ToObject()), __VA_ARGS__)

#define POSSIBLY_EMPTY_CHILD(x, ...)                                                \
    if (node.ElementExists(x)) {                                                    \
        if (!node[x]->IsObject()) THROW_SANITY_EXCEPTION("Child is not an object"); \
        CHILDREN_IS_TYPE_OF(x, __VA_ARGS__);                                        \
    }

#define THROW_SANITY_EXCEPTION(msg) throw SanityException(msg)

template<class... A>
void CheckChildren(const Object &node, A... args) {
    std::string items[sizeof...(args)] = { args... };
    for (auto item : items) {
        if (!node.ElementExists(item) || !node[item]->IsObject()) throw SanityException("Child " + item + " is not valid");
    }
}

template<class... A>
void HasElement(const Object &node, A... args) {
    std::string items[sizeof...(args)] = { args... };
    for (auto item : items) {
        if (!node.ElementExists(item)) throw SanityException("Element " + item + " does not exist");
    }
}

template<class... A>
void ChildrenType(const Object &node, A... args) {
    std::string items[sizeof...(args)] = { args... };
    auto type = node[AST_TAG_TYPE_KEY]->ToString();
    for (auto item : items) {
        if (type == item) return;
    }
    throw SanityException("Child node does not have expected type. Found " + type);
}

void SanityVisitor::VisitProgram(const Object &node) {
    CHECK_TYPE(AST_TAG_PROGRAM);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_STMTS);
}

void SanityVisitor::VisitStatements(const Object &node) {
    CHECK_TYPE(AST_TAG_STMTS);
}

void SanityVisitor::VisitStatement(const Object &node) {
    CHECK_TYPE(AST_TAG_STMT);

    POSSIBLY_EMPTY_CHILD(AST_TAG_CHILD,
                         AST_TAG_EXPR, AST_TAG_IF, AST_TAG_WHILE, AST_TAG_FOR, AST_TAG_RETURN,
                         AST_TAG_BREAK, AST_TAG_CONTINUE, AST_TAG_BLOCK,
                         AST_TAG_FUNCTION_DEF, AST_TAG_EMPTY);
}

void SanityVisitor::VisitExpression(const Object &node) {
    CHECK_TYPE(AST_TAG_EXPR);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD,
                        AST_TAG_ASSIGN, AST_TAG_PLUS, AST_TAG_MINUS, AST_TAG_MUL,
                        AST_TAG_DIV, AST_TAG_MODULO, AST_TAG_GREATER, AST_TAG_LESS,
                        AST_TAG_GEQUAL, AST_TAG_LEQUAL, AST_TAG_EQUAL, AST_TAG_NEQUAL,
                        AST_TAG_AND, AST_TAG_OR, AST_TAG_TERM);
}

void SanityVisitor::VisitAssign(const Object &node) {
    CHECK_TYPE(AST_TAG_ASSIGN);

    CHECK_CHILDREN(AST_TAG_LVALUE, AST_TAG_RVALUE);

    CHILDREN_IS_TYPE_OF(AST_TAG_LVALUE, AST_TAG_LVALUE);
    CHILDREN_IS_TYPE_OF(AST_TAG_RVALUE, AST_TAG_EXPR);
}

void SanityVisitor::VisitPlus(const Object &node) {
    CHECK_TYPE(AST_TAG_PLUS);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitMinus(const Object &node) {
    CHECK_TYPE(AST_TAG_MINUS);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitMul(const Object &node) {
    CHECK_TYPE(AST_TAG_MUL);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitDiv(const Object &node) {
    CHECK_TYPE(AST_TAG_DIV);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitModulo(const Object &node) {
    CHECK_TYPE(AST_TAG_MODULO);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitGreater(const Object &node) {
    CHECK_TYPE(AST_TAG_GREATER);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitLess(const Object &node) {
    CHECK_TYPE(AST_TAG_LESS);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitGreaterEqual(const Object &node) {
    CHECK_TYPE(AST_TAG_GEQUAL);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitLessEqual(const Object &node) {
    CHECK_TYPE(AST_TAG_LEQUAL);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitEqual(const Object &node) {
    CHECK_TYPE(AST_TAG_EQUAL);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitNotEqual(const Object &node) {
    CHECK_TYPE(AST_TAG_NEQUAL);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitAnd(const Object &node) {
    CHECK_TYPE(AST_TAG_AND);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitOr(const Object &node) {
    CHECK_TYPE(AST_TAG_OR);

    CHECK_CHILDREN(AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_FIRST_EXPR, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_SECOND_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitTerm(const Object &node) {
    CHECK_TYPE(AST_TAG_TERM);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD,
                        AST_TAG_EXPR, AST_TAG_MINUS, AST_TAG_NOT, AST_TAG_BPLUSPLUS,
                        AST_TAG_APLUSPLUS, AST_TAG_BMINUSMINUS, AST_TAG_AMINUSMINUS, AST_TAG_PRIMARY);
}

void SanityVisitor::VisitUnaryMinus(const Object &node) {
    CHECK_TYPE(AST_TAG_UMINUS);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_EXPR);
}

void SanityVisitor::VisitNot(const Object &node) {
    CHECK_TYPE(AST_TAG_NOT);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_EXPR);
}

void SanityVisitor::VisitPlusPlusBefore(const Object &node) {
    CHECK_TYPE(AST_TAG_BPLUSPLUS);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_LVALUE);
}

void SanityVisitor::VisitPlusPlusAfter(const Object &node) {
    CHECK_TYPE(AST_TAG_APLUSPLUS);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_LVALUE);
}

void SanityVisitor::VisitMinusMinusBefore(const Object &node) {
    CHECK_TYPE(AST_TAG_BMINUSMINUS);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_LVALUE);
}

void SanityVisitor::VisitMinusMinusAfter(const Object &node) {
    CHECK_TYPE(AST_TAG_AMINUSMINUS);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_LVALUE);
}

void SanityVisitor::VisitPrimary(const Object &node) {
    CHECK_TYPE(AST_TAG_PRIMARY);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD,
                        AST_TAG_LVALUE, AST_TAG_CALL, AST_TAG_OBJECT_DEF, AST_TAG_FUNCTION_DEF,
                        AST_TAG_CONST, AST_TAG_META_PARSE, AST_TAG_META_UNPARSE,
                        AST_TAG_META_INLINE, AST_TAG_META_ESCAPE, AST_TAG_META_QUASI_QUOTES);
}

void SanityVisitor::VisitMetaParse(const Object &node) {
    CHECK_TYPE(AST_TAG_META_PARSE);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_EXPR);
}

void SanityVisitor::VisitMetaUnparse(const Object &node) {
    CHECK_TYPE(AST_TAG_META_UNPARSE);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_EXPR);
}

void SanityVisitor::VisitMetaInline(const Object &node) {
    CHECK_TYPE(AST_TAG_META_INLINE);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_EXPR);
}

void SanityVisitor::VisitMetaEscape(const Object &node) {
    CHECK_TYPE(AST_TAG_META_ESCAPE);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_EXPR, AST_TAG_ID);
}

void SanityVisitor::VisitMetaQuasiQuotes(const Object &node) {
    CHECK_TYPE(AST_TAG_META_QUASI_QUOTES);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_STMTS, AST_TAG_ELIST);
}

void SanityVisitor::VisitRuntimeEmpty(const Object &node) {
    CHECK_TYPE(AST_TAG_RUNTIME_EMPTY);
}

void SanityVisitor::VisitLValue(const Object &node) {
    CHECK_TYPE(AST_TAG_LVALUE);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD,
                        AST_TAG_ID, AST_TAG_DOLLAR_ID, AST_TAG_LOCAL_ID,
                        AST_TAG_DOUBLECOLON_ID, AST_TAG_DOLLAR_LAMBDA, AST_TAG_DOLLAR_ENV,
                        AST_TAG_DOLLAR_ID, AST_TAG_MEMBER);
}

void SanityVisitor::VisitId(const Object &node) {
    CHECK_TYPE(AST_TAG_ID);

    HAS_ELEMENTS(AST_TAG_ID);

    if (!node[AST_TAG_ID]->IsString()) THROW_SANITY_EXCEPTION("ID is not a string. Found " + node[AST_TAG_ID]->GetTypeToString());
}

void SanityVisitor::VisitLocal(const Object &node) {
    CHECK_TYPE(AST_TAG_LOCAL_ID);
}

void SanityVisitor::VisitDoubleColon(const Object &node) {
    CHECK_TYPE(AST_TAG_DOUBLECOLON_ID);
}

void SanityVisitor::VisitDollar(const Object &node) {
    CHECK_TYPE(AST_TAG_DOLLAR_ID);
}

void SanityVisitor::VisitDollarEnv(const Object &node) {
    CHECK_TYPE(AST_TAG_DOLLAR_ENV);
}

void SanityVisitor::VisitDollarLambda(const Object &node) {
    CHECK_TYPE(AST_TAG_DOLLAR_LAMBDA);
}

void SanityVisitor::VisitMember(const Object &node) {
    CHECK_TYPE(AST_TAG_MEMBER);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_DOT, AST_TAG_BRACKET);
}

void SanityVisitor::VisitDot(const Object &node) {
    CHECK_TYPE(AST_TAG_DOT);

    CHECK_CHILDREN(AST_TAG_LVALUE, AST_TAG_ID);

    CHILDREN_IS_TYPE_OF(AST_TAG_LVALUE, AST_TAG_LVALUE, AST_TAG_CALL);
    CHILDREN_IS_TYPE_OF(AST_TAG_ID, AST_TAG_ID, AST_TAG_DOLLAR_ID);
}

void SanityVisitor::VisitBracket(const Object &node) {
    CHECK_TYPE(AST_TAG_BRACKET);

    CHECK_CHILDREN(AST_TAG_LVALUE, AST_TAG_EXPR);

    CHILDREN_IS_TYPE_OF(AST_TAG_LVALUE, AST_TAG_LVALUE, AST_TAG_CALL);
    CHILDREN_IS_TYPE_OF(AST_TAG_EXPR, AST_TAG_EXPR);
}

void SanityVisitor::VisitCall(const Object &node) {
    CHECK_TYPE(AST_TAG_CALL);

    CHECK_CHILDREN(AST_TAG_FUNCTION, AST_TAG_ARGUMENTS);

    CHILDREN_IS_TYPE_OF(AST_TAG_FUNCTION,
                        AST_TAG_CALL, AST_TAG_METHOD_CALL, AST_TAG_ID,
                        AST_TAG_LVALUE, AST_TAG_FUNCTION_DEF);
}

void SanityVisitor::VisitArgumentList(const Object &node) {
    CHECK_TYPE(AST_TAG_ARGLIST);
}

void SanityVisitor::VisitNamedArgument(const Object &node) {
    CHECK_TYPE(AST_TAG_NAMED);

    CHECK_CHILDREN(AST_TAG_NAMED_KEY, AST_TAG_NAMED_VALUE);

    CHILDREN_IS_TYPE_OF(AST_TAG_NAMED_KEY, AST_TAG_ID);
    CHILDREN_IS_TYPE_OF(AST_TAG_NAMED_VALUE, AST_TAG_EXPR);
}

void SanityVisitor::VisitExpressionList(const Object &node) {
    CHECK_TYPE(AST_TAG_ELIST);
}

void SanityVisitor::VisitObjectDef(const Object &node) {
    CHECK_TYPE(AST_TAG_OBJECT_DEF);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_ELIST, AST_TAG_INDEXED);
}

void SanityVisitor::VisitIndexed(const Object &node) {
    CHECK_TYPE(AST_TAG_INDEXED);
}

void SanityVisitor::VisitIndexedElem(const Object &node) {
    CHECK_TYPE(AST_TAG_INDEXED_ELEM);

    CHECK_CHILDREN(AST_TAG_OBJECT_KEY, AST_TAG_OBJECT_VALUE);
}

void SanityVisitor::VisitBlock(const Object &node) {
    CHECK_TYPE(AST_TAG_BLOCK);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD, AST_TAG_STMTS);
}

void SanityVisitor::VisitFunctionDef(const Object &node) {
    CHECK_TYPE(AST_TAG_FUNCTION_DEF);

    CHECK_CHILDREN(AST_TAG_FUNCTION_ID, AST_TAG_FUNCTION_FORMALS, AST_TAG_STMT);

    CHILDREN_IS_TYPE_OF(AST_TAG_FUNCTION_ID, AST_TAG_ID);
    CHILDREN_IS_TYPE_OF(AST_TAG_FUNCTION_FORMALS, AST_TAG_ID_LIST);
    CHILDREN_IS_TYPE_OF(AST_TAG_STMT, AST_TAG_BLOCK);
}

void SanityVisitor::VisitConst(const Object &node) {
    CHECK_TYPE(AST_TAG_CONST);

    CHECK_CHILDREN(AST_TAG_CHILD);

    CHILDREN_IS_TYPE_OF(AST_TAG_CHILD,
                        AST_TAG_NUMBER, AST_TAG_STRING, AST_TAG_NIL,
                        AST_TAG_TRUE, AST_TAG_FALSE);
}

void SanityVisitor::VisitNumber(const Object &node) {
    CHECK_TYPE(AST_TAG_NUMBER);

    HAS_ELEMENTS(AST_TAG_VALUE);

    if (!node[AST_TAG_VALUE]->IsNumber()) THROW_SANITY_EXCEPTION("AST Number node does not contain number value");
}

void SanityVisitor::VisitString(const Object &node) {
    CHECK_TYPE(AST_TAG_STRING);

    HAS_ELEMENTS(AST_TAG_VALUE);

    if (!node[AST_TAG_VALUE]->IsString()) THROW_SANITY_EXCEPTION("AST String node does not contain string value");
}

void SanityVisitor::VisitNil(const Object &node) {
    CHECK_TYPE(AST_TAG_NIL);

    HAS_ELEMENTS(AST_TAG_VALUE);

    // TBD: following condition must always be true but it's not necessery
    // if(!node[AST_TAG_VALUE]->IsNil()) THROW_SANITY_EXCEPTION;
}

void SanityVisitor::VisitTrue(const Object &node) {
    CHECK_TYPE(AST_TAG_TRUE);

    HAS_ELEMENTS(AST_TAG_VALUE);

    if (!node[AST_TAG_VALUE]->IsBoolean() || !node[AST_TAG_VALUE]->ToBoolean() == true) THROW_SANITY_EXCEPTION("AST Boolean node does not conaint valid value");
}

void SanityVisitor::VisitFalse(const Object &node) {
    CHECK_TYPE(AST_TAG_FALSE);

    HAS_ELEMENTS(AST_TAG_VALUE);

    if (!node[AST_TAG_VALUE]->IsBoolean() || !node[AST_TAG_VALUE]->ToBoolean() == false) THROW_SANITY_EXCEPTION("AST Boolean node does not conaint valid value");
}

void SanityVisitor::VisitIdList(const Object &node) {
    CHECK_TYPE(AST_TAG_ID_LIST);
}

void SanityVisitor::VisitFormal(const Object &node) {
    CHECK_TYPE(AST_TAG_FORMAL);
}

void SanityVisitor::VisitIf(const Object &node) {
    CHECK_TYPE(AST_TAG_IF);

    CHECK_CHILDREN(AST_TAG_CONDITION, AST_TAG_STMT);

    CHILDREN_IS_TYPE_OF(AST_TAG_CONDITION, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_STMT, AST_TAG_STMT);
}

void SanityVisitor::VisitWhile(const Object &node) {
    CHECK_TYPE(AST_TAG_WHILE);

    CHECK_CHILDREN(AST_TAG_CONDITION, AST_TAG_STMT);

    CHILDREN_IS_TYPE_OF(AST_TAG_CONDITION, AST_TAG_EXPR);
    CHILDREN_IS_TYPE_OF(AST_TAG_STMT, AST_TAG_STMT);
}

void SanityVisitor::VisitFor(const Object &node) {
    CHECK_TYPE(AST_TAG_FOR);

    CHECK_CHILDREN(
        AST_TAG_FOR_POST_ELIST,
        AST_TAG_CONDITION,
        AST_TAG_FOR_POST_ELIST,
        AST_TAG_STMT);
}

void SanityVisitor::VisitReturn(const Object &node) {
    CHECK_TYPE(AST_TAG_RETURN);
}

void SanityVisitor::VisitBreak(const Object &node) {
    CHECK_TYPE(AST_TAG_BREAK);
}

void SanityVisitor::VisitContinue(const Object &node) {
    CHECK_TYPE(AST_TAG_CONTINUE);
}

/* TreeVisitor *SanityVisitor::Clone(void) const {
    // TO BE DELETED
    return nullptr;
}

SanityVisitor::SanityVisitor(void) {}

SanityVisitor::~SanityVisitor() {
}
 */
