#include "AnonymousFuncsVisitor.h"
#include "TreeTags.h"

#include <cassert>

bool IsAnonymousFunctionNode(const Object * node) {
    assert((*node)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ID);

    std::string name = (*node)[AST_TAG_ID]->ToString();

    return (name[0] == '$');
}

TreeVisitor * AnonymousFuncsVisitor::Clone(void) const {
    return new AnonymousFuncsVisitor();
}

void AnonymousFuncsVisitor::SetNameOffset(unsigned num) {
    nameOffset = num;
}

unsigned AnonymousFuncsVisitor::GetNameOffset(void) {
    return nameOffset;
}

#include <iostream>
void AnonymousFuncsVisitor::VisitFunctionDef(const Object &node) {
    assert(node[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FUNCTION_DEF);

    Object *child = node[AST_TAG_FUNCTION_ID]->ToObject_NoConst();
    assert((*child)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_ID);

    std::string name = (*child)[AST_TAG_ID]->ToString();

    if (IsAnonymousFunctionNode(child)) {
        child->Set(AST_TAG_ID, '$' + std::to_string(nameOffset++));
    }
}