#include "EvalMetaEscapesTreeVisitor.h"

#include <cassert>

void EvalMetaEscapesTreeVisitor::SetEvalMetaEscape(EvalMetaEscape callback) {
    assert(callback);
    evalMetaEscape = callback;
}
