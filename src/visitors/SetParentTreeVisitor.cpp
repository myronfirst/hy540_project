#include <assert.h>
#include "HiddenTags.h"
#include "SetParentTreeVisitor.h"

void SetParentTreeVisitor::SetParent(const Object &parent, unsigned children) {
    Object &parentNoConst = const_cast<Object &>(parent);
    assert(parent.IsValid());
    if (isRemoval) {
        Object* p = parentNoConst[PARENT_RESERVED_FIELD]->ToObject_NoConst();
        parentNoConst.Remove(PARENT_RESERVED_FIELD);
        p->DecreaseRefCounter();
        return;
    }
    for (register unsigned i = 0; i < children; ++i){
        Object* child = valueStack.GetTopAndPop().ToObject_NoConst();
        
        const Value* prevParentVal = (*child)[PARENT_RESERVED_FIELD]; 
        Object* prevParent = prevParentVal ? prevParentVal->ToObject_NoConst() : nullptr;
        
        child->Set(PARENT_RESERVED_FIELD, Value(&parentNoConst));

        assert(parentNoConst.GetReferences() > 0);
        
        parentNoConst.IncreaseRefCounter();
        if (prevParent)
            prevParent->DecreaseRefCounter();
    }

    valueStack.Push(Value(&parentNoConst));
}
