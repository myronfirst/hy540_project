#include "ValidityVisitor.h"

#include "Object.h"
#include "TreeTags.h"
#include "Utilities.h"

#include <cassert>
#include <iostream>

#include "HiddenTags.h"

void ValidityVisitor::VisitDollarLambda(const Object &node) {
    const Object *parent = node[PARENT_RESERVED_FIELD]->ToObject();
    while ((*parent)[PARENT_RESERVED_FIELD]) {
        if ((*parent)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_FUNCTION_DEF ||
            (*parent)[AST_TAG_TYPE_KEY]->ToString() == AST_TAG_META_QUASI_QUOTES) {
            return;
        }
        parent = (*parent)[PARENT_RESERVED_FIELD]->ToObject();
    }
    Utilities::SyntaxError("$lambda can only be used inside of a function");
}

void ValidityVisitor::VisitReturn(const Object &node) {
    auto current = *node[PARENT_RESERVED_FIELD]->ToObject();
    while (current[PARENT_RESERVED_FIELD]) {
        auto type = current[AST_TAG_TYPE_KEY]->ToString();
        if (type == AST_TAG_FUNCTION_DEF || type == AST_TAG_META_QUASI_QUOTES) return;
        current = *current[PARENT_RESERVED_FIELD]->ToObject();
        assert(current.IsValid());
    }
    Utilities::SyntaxError("Return can only be used inside of a function");
}

void ValidityVisitor::VisitBreak(const Object &node) {
    auto current = *node[PARENT_RESERVED_FIELD]->ToObject();
    while (current[PARENT_RESERVED_FIELD] && current[AST_TAG_TYPE_KEY]->ToString() != AST_TAG_FUNCTION_DEF) {
        auto type = current[AST_TAG_TYPE_KEY]->ToString();
        if (type == AST_TAG_FOR || type == AST_TAG_WHILE || type == AST_TAG_META_QUASI_QUOTES) return;
        current = *current[PARENT_RESERVED_FIELD]->ToObject();
        assert(current.IsValid());
    }
    Utilities::SyntaxError("Break can only be used inside of a loop");
}

void ValidityVisitor::VisitContinue(const Object &node) {
    auto current = *node[PARENT_RESERVED_FIELD]->ToObject();
    while (current[PARENT_RESERVED_FIELD] && current[AST_TAG_TYPE_KEY]->ToString() != AST_TAG_FUNCTION_DEF) {
        auto type = current[AST_TAG_TYPE_KEY]->ToString();
        if (type == AST_TAG_FOR || type == AST_TAG_WHILE || type == AST_TAG_META_QUASI_QUOTES) return;
        current = *current[PARENT_RESERVED_FIELD]->ToObject();
        assert(current.IsValid());
    }
    Utilities::SyntaxError("Continue can only be used inside of a loop");
}

void ValidityVisitor::VisitMetaEscape(const Object &node){
    auto current = *node[PARENT_RESERVED_FIELD]->ToObject();
    while (current[PARENT_RESERVED_FIELD] && current[AST_TAG_TYPE_KEY]->ToString() != AST_TAG_META_QUASI_QUOTES) {
        current = *current[PARENT_RESERVED_FIELD]->ToObject();
        assert(current.IsValid());
    }

    if (current[AST_TAG_TYPE_KEY]->ToString() != AST_TAG_META_QUASI_QUOTES)
        Utilities::SyntaxError("Meta escape (\"~\") can only be used inside quasi quotes");
}