#include "Tags.alpha"

local acceptors = [];
local visitor = nil;

function Accept(node) {
    assert(typeof (node) == "object");
    assert(typeof (node[AST_TAG_TYPE_KEY] == "string"));
    acceptors[node[AST_TAG_TYPE_KEY]](node);
}

function TreeHost(ast, visitor_) {
    assert(typeof(ast) == "object");
    assert(typeof(visitor_) == "object");
    visitor = visitor_;
    Accept(ast);
}

function InstallAcceptor(tag, func) {
    assert(typeof (tag) == "string");
    assert(typeof (func) == "program function");
    acceptors[tag] = func;
}

function AssertType(node, type) {
    assert(typeof (node) == "object");
    assert(typeof (type) == "string");

    assert(typeof (node[AST_TAG_TYPE_KEY]) == "string");
    assert(node[AST_TAG_TYPE_KEY] == type);
}

function AcceptProgram(node) {
    AssertType(node, AST_TAG_PROGRAM);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitProgram(node);
}

function AcceptStatements(node) {
    AssertType(node, AST_TAG_STMTS);

    for (i = 0; node[i] != nil; ++i)
        Accept(node[i]);

    visitor.VisitStatements(node);
}

function AcceptStatement(node) {
    AssertType(node, AST_TAG_STMT);

    if (node[AST_TAG_CHILD] != nil)
        Accept(node[AST_TAG_CHILD]);

    visitor.VisitStatement(node);
}

function AcceptExpression(node) {
    AssertType(node, AST_TAG_EXPR);

    Accept(node[AST_TAG_CHILD]);
    visitor.VisitExpression(node);
}

function AcceptAssign(node) {
    AssertType(node, AST_TAG_ASSIGN);
    Accept(node[AST_TAG_LVALUE]);
    Accept(node[AST_TAG_RVALUE]);
    visitor.VisitAssign(node);
}

function AcceptPlus(node) {
    AssertType(node, AST_TAG_PLUS);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitPlus(node);
}

function AcceptMinus(node) {
    AssertType(node, AST_TAG_MINUS);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitMinus(node);
}

function AcceptMul(node) {
    AssertType(node, AST_TAG_MUL);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitMul(node);
}

function AcceptDiv(node) {
    AssertType(node, AST_TAG_DIV);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitDiv(node);
}

function AcceptModulo(node) {
    AssertType(node, AST_TAG_MODULO);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitModulo(node);
}

function AcceptGreater(node) {
    AssertType(node, AST_TAG_GREATER);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitGreater(node);
}

function AcceptLess(node) {
    AssertType(node, AST_TAG_LESS);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitLess(node);
}

function AcceptGreaterEqual(node) {
    AssertType(node, AST_TAG_GEQUAL);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitGreaterEqual(node);
}

function AcceptLessEqual(node) {
    AssertType(node, AST_TAG_LEQUAL);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitLessEqual(node);
}

function AcceptEqual(node) {
    AssertType(node, AST_TAG_EQUAL);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitEqual(node);
}

function AcceptNotEqual(node) {
    AssertType(node, AST_TAG_NEQUAL);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitNotEqual(node);
}

function AcceptAnd(node) {
    AssertType(node, AST_TAG_AND);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitAnd(node);
}

function AcceptOr(node) {
    AssertType(node, AST_TAG_OR);
    Accept(node[AST_TAG_FIRST_EXPR]);
    Accept(node[AST_TAG_SECOND_EXPR]);
    visitor.VisitOr(node);
}

function AcceptTerm(node) {
    AssertType(node, AST_TAG_TERM);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitTerm(node);
}

function AcceptUnaryMinus(node) {
    AssertType(node, AST_TAG_UMINUS);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitUnaryMinus(node);
}

function AcceptNot(node) {
    AssertType(node, AST_TAG_NOT);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitNot(node);
}

function AcceptPlusPlusBefore(node) {
    AssertType(node, AST_TAG_BPLUSPLUS);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitPlusPlusBefore(node);
}

function AcceptPlusPlusAfter(node) {
    AssertType(node, AST_TAG_APLUSPLUS);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitPlusPlusAfter(node);
}

function AcceptMinusMinusBefore(node) {
    AssertType(node, AST_TAG_BMINUSMINUS);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitMinusMinusBefore(node);
}

function AcceptMinusMinusAfter(node) {
    AssertType(node, AST_TAG_AMINUSMINUS);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitMinusMinusAfter(node);
}

function AcceptPrimary(node) {
    AssertType(node, AST_TAG_PRIMARY);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitPrimary(node);
}

function AcceptLValue(node) {
    AssertType(node, AST_TAG_LVALUE);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitLValue(node);
}

function AcceptId(node) {
    AssertType(node, AST_TAG_ID);
    assert(node[AST_TAG_ID] != nil);
    visitor.VisitId(node);
}

function AcceptLocal(node) {
    AssertType(node, AST_TAG_LOCAL_ID);
    assert(node[AST_TAG_ID] != nil);
    visitor.VisitLocal(node);
}

function AcceptDoubleColon(node) {
    AssertType(node, AST_TAG_DOUBLECOLON_ID);
    assert(node[AST_TAG_ID] != nil);
    visitor.VisitDoubleColon(node);
}

function AcceptDollar(node) {
    AssertType(node, AST_TAG_DOLLAR_ID);
    assert(node[AST_TAG_ID] != nil);
    visitor.VisitDollar(node);
}

function AcceptDollarEnv(node) {
    AssertType(node, AST_TAG_DOLLAR_ENV);
    assert(node[AST_TAG_ID] != nil);
    visitor.VisitDollarEnv(node);
}

function AcceptDollarLambda(node) {
    AssertType(node, AST_TAG_DOLLAR_LAMBDA);
    assert(node[AST_TAG_ID] != nil);
    visitor.VisitDollarLambda(node);
}

function AcceptMember(node) {
    AssertType(node, AST_TAG_MEMBER);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitMember(node);
}

function AcceptDot(node) {
    AssertType(node, AST_TAG_DOT);
    Accept(node[AST_TAG_LVALUE]);
    Accept(node[AST_TAG_ID]);
    visitor.VisitDot(node);
}

function AcceptBracket(node) {
    AssertType(node, AST_TAG_BRACKET);
    Accept(node[AST_TAG_LVALUE]);
    Accept(node[AST_TAG_EXPR]);
    visitor.VisitBracket(node);
}

function AcceptCall(node) {
    AssertType(node, AST_TAG_CALL);

    if (node[AST_TAG_LVALUE] != nil)
        Accept(node[AST_TAG_LVALUE]);

    Accept(node[AST_TAG_FUNCTION]);
    Accept(node[AST_TAG_ARGUMENTS]);
    visitor.VisitCall(node);
}

function AcceptArgumentList(node) {
    AssertType(node, AST_TAG_ARGLIST);

    for (i = 0; node[i] != nil; ++i)
        Accept(node[i]);

    visitor.VisitArgumentList(node);
}

function AcceptNamedArgument(node) {
    AssertType(node, AST_TAG_NAMED);
    Accept(node[AST_TAG_NAMED_KEY]);
    Accept(node[AST_TAG_NAMED_VALUE]);
    visitor.VisitNamedArgument(node);
}

function AcceptExpressionList(node) {
    AssertType(node, AST_TAG_ELIST);

    for (i = 0; node[i] != nil; ++i)
        Accept(node[i]);

    visitor.VisitExpressionList(node);
}

function AcceptObjectDef(node) {
    AssertType(node, AST_TAG_OBJECT_DEF);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitObjectDef(node);
}

function AcceptIndexed(node) {
    AssertType(node, AST_TAG_INDEXED);

    for (i = 0; node[i] != nil; ++i)
        Accept(node[i]);

    visitor.VisitIndexed(node);
}

function AcceptIndexedElem(node) {
    AssertType(node, AST_TAG_INDEXED_ELEM);
    Accept(node[AST_TAG_OBJECT_KEY]);
    Accept(node[AST_TAG_OBJECT_VALUE]);
    visitor.VisitIndexedElem(node);
}

function AcceptBlock(node) {
    AssertType(node, AST_TAG_BLOCK);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitBlock(node);
}

function AcceptFunctionDef(node) {
    AssertType(node, AST_TAG_FUNCTION_DEF);
    Accept(node[AST_TAG_FUNCTION_ID]);
    Accept(node[AST_TAG_FUNCTION_FORMALS]);
    Accept(node[AST_TAG_STMT]);
    visitor.VisitFunctionDef(node);
}

function AcceptConst(node) {
    AssertType(node, AST_TAG_CONST);
    Accept(node[AST_TAG_CHILD]);
    visitor.VisitConst(node);
}

function AcceptNumber(node) {
    AssertType(node, AST_TAG_NUMBER);
    assert(node[AST_TAG_VALUE] != nil);
    visitor.VisitNumber(node);
}

function AcceptString(node) {
    AssertType(node, AST_TAG_STRING);
    assert(node[AST_TAG_VALUE] != nil);
    visitor.VisitString(node);
}

function AcceptNil(node) {
    AssertType(node, AST_TAG_NIL);
    visitor.VisitNil(node);
}

function AcceptTrue(node) {
    AssertType(node, AST_TAG_TRUE);
    visitor.VisitTrue(node);
}

function AcceptFalse(node) {
    AssertType(node, AST_TAG_FALSE);
    visitor.VisitFalse(node);
}

function AcceptIdList(node) {
    AssertType(node, AST_TAG_ID_LIST);

    for (i = 0; node[i] != nil; ++i)
        Accept(node[i]);

    visitor.VisitIdList(node);
}

function AcceptIf(node) {
    AssertType(node, AST_TAG_IF);
    Accept(node[AST_TAG_CONDITION]);
    Accept(node[AST_TAG_STMT]);

    if (node[AST_TAG_ELSE_STMT] != nil)
        Accept(node[AST_TAG_ELSE_STMT]);

    visitor.VisitIf(node);
}

function AcceptWhile(node) {
    AssertType(node, AST_TAG_WHILE);
    Accept(node[AST_TAG_CONDITION]);
    Accept(node[AST_TAG_STMT]);

    visitor.VisitWhile(node);
}

function AcceptFor(node) {
    AssertType(node, AST_TAG_FOR);
    Accept(node[AST_TAG_FOR_PRE_ELIST]);
    Accept(node[AST_TAG_CONDITION]);
    Accept(node[AST_TAG_FOR_POST_ELIST]);
    Accept(node[AST_TAG_STMT]);

    visitor.VisitFor(node);
}

function AcceptReturn(node) {
    AssertType(node, AST_TAG_RETURN);

    if (node[AST_TAG_CHILD] != nil)
        Accept(node[AST_TAG_CHILD]);

    visitor.VisitReturn(node);
}

function AcceptBreak(node) {
    AssertType(node, AST_TAG_BREAK);
    visitor.VisitBreak(node);
}

function AcceptContinue(node) {
    AssertType(node, AST_TAG_CONTINUE);
    visitor.VisitBreak(node);
}

InstallAcceptor(AST_TAG_PROGRAM, (AcceptProgram));
InstallAcceptor(AST_TAG_STMTS, (AcceptStatements));
InstallAcceptor(AST_TAG_STMT, (AcceptStatement));
InstallAcceptor(AST_TAG_EXPR, (AcceptExpression));
InstallAcceptor(AST_TAG_ASSIGN, (AcceptAssign));
InstallAcceptor(AST_TAG_PLUS, (AcceptPlus));
InstallAcceptor(AST_TAG_MINUS, (AcceptMinus));
InstallAcceptor(AST_TAG_MUL, (AcceptMul));
InstallAcceptor(AST_TAG_DIV, (AcceptDiv));
InstallAcceptor(AST_TAG_MODULO, (AcceptModulo));
InstallAcceptor(AST_TAG_GREATER, (AcceptGreater));
InstallAcceptor(AST_TAG_LESS, (AcceptLess));
InstallAcceptor(AST_TAG_GEQUAL, (AcceptGreaterEqual));
InstallAcceptor(AST_TAG_LEQUAL, (AcceptLessEqual));
InstallAcceptor(AST_TAG_EQUAL, (AcceptEqual));
InstallAcceptor(AST_TAG_NEQUAL, (AcceptNotEqual));
InstallAcceptor(AST_TAG_AND, (AcceptAnd));
InstallAcceptor(AST_TAG_OR, (AcceptOr));
InstallAcceptor(AST_TAG_TERM, (AcceptTerm));
InstallAcceptor(AST_TAG_UMINUS, (AcceptUnaryMinus));
InstallAcceptor(AST_TAG_NOT, (AcceptNot));
InstallAcceptor(AST_TAG_BPLUSPLUS, (AcceptPlusPlusBefore));
InstallAcceptor(AST_TAG_APLUSPLUS, (AcceptPlusPlusAfter));
InstallAcceptor(AST_TAG_BMINUSMINUS, (AcceptMinusMinusBefore));
InstallAcceptor(AST_TAG_AMINUSMINUS, (AcceptMinusMinusAfter));
InstallAcceptor(AST_TAG_PRIMARY, (AcceptPrimary));
InstallAcceptor(AST_TAG_LVALUE, (AcceptLValue));
InstallAcceptor(AST_TAG_ID, (AcceptId));
InstallAcceptor(AST_TAG_LOCAL_ID, (AcceptLocal));
InstallAcceptor(AST_TAG_DOUBLECOLON_ID, (AcceptDoubleColon));
InstallAcceptor(AST_TAG_DOLLAR_ID, (AcceptDollar));
InstallAcceptor(AST_TAG_DOLLAR_LAMBDA, (AcceptDollarLambda));
InstallAcceptor(AST_TAG_DOLLAR_ENV, (AcceptDollarEnv));
InstallAcceptor(AST_TAG_MEMBER, (AcceptMember));
InstallAcceptor(AST_TAG_DOT, (AcceptDot));
InstallAcceptor(AST_TAG_BRACKET, (AcceptBracket));
InstallAcceptor(AST_TAG_CALL, (AcceptCall));
InstallAcceptor(AST_TAG_ARGLIST, (AcceptArgumentList));
InstallAcceptor(AST_TAG_NAMED, (AcceptNamedArgument));
InstallAcceptor(AST_TAG_ELIST, (AcceptExpressionList));
InstallAcceptor(AST_TAG_OBJECT_DEF, (AcceptObjectDef));
InstallAcceptor(AST_TAG_INDEXED, (AcceptIndexed));
InstallAcceptor(AST_TAG_INDEXED_ELEM, (AcceptIndexedElem));
InstallAcceptor(AST_TAG_BLOCK, (AcceptBlock));
InstallAcceptor(AST_TAG_FUNCTION_DEF, (AcceptFunctionDef));
InstallAcceptor(AST_TAG_CONST, (AcceptConst));
InstallAcceptor(AST_TAG_NUMBER, (AcceptNumber));
InstallAcceptor(AST_TAG_STRING, (AcceptString));
InstallAcceptor(AST_TAG_NIL, (AcceptNil));
InstallAcceptor(AST_TAG_TRUE, (AcceptTrue));
InstallAcceptor(AST_TAG_FALSE, (AcceptFalse));
InstallAcceptor(AST_TAG_ID_LIST, (AcceptIdList));
InstallAcceptor(AST_TAG_IF, (AcceptIf));
InstallAcceptor(AST_TAG_WHILE, (AcceptWhile));
InstallAcceptor(AST_TAG_FOR, (AcceptFor));
InstallAcceptor(AST_TAG_RETURN, (AcceptReturn));
InstallAcceptor(AST_TAG_BREAK, (AcceptBreak));
InstallAcceptor(AST_TAG_CONTINUE, (AcceptContinue));