EXECUTABLE='./interpreter.out'
DIFF="diff"
DIFF_NO_WS="diff --ignore-space-at-eol"

TESTS_DIR=./tests/unit_tests

OUTPUT_FILE=out.tmp

BOLD_FACE="\033[1m"
GREEN_COLOR="\033[32;1m"
RED_COLOR="\033[31;1m"
NO_COLOR="\033[0m"

PASSED_TESTS=0
FAILED_TESTS=0
TOTAL_TESTS=0

echo $BOLD_FACE"Testing..." $NO_COLOR

for i in $TESTS_DIR/in/*;
do
    file=${i##*/}
    filename=${file%.*}

    TOTAL_TESTS=$((TOTAL_TESTS+1))

    ${EXECUTABLE} $i > ${OUTPUT_FILE} 2>&1
    ${DIFF} ${TESTS_DIR}/out/${filename}.output ${OUTPUT_FILE}
    # check the diff exit code
    RES=$?
    if [ $RES -eq 0 ] # if there are no diffs between the files
    then
        echo $GREEN_COLOR "PASS" $NO_COLOR $filename
        PASSED_TESTS=$((PASSED_TESTS+1))
    elif [ $RES -eq 1 ] # if there are diffs between the files
    then
        echo $RED_COLOR "FAIL" $NO_COLOR $filename
        FAILED_TESTS=$((FAILED_TESTS+1))
    else
        echo  "Automated testing failed" $filename
    fi
    rm -f ${OUTPUT_FILE} || true
done

echo ""
echo $BOLD_FACE"Results" $NO_COLOR
echo "Passed Tests:" $GREEN_COLOR ${PASSED_TESTS} $NO_COLOR / ${TOTAL_TESTS}
echo "Failed Tests:" $RED_COLOR ${FAILED_TESTS} $NO_COLOR / ${TOTAL_TESTS}