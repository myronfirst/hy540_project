EXECUTABLE='./interpreter.out'

TESTS_DIR=./tests/unit_tests

OUTPUT_FILE=out.tmp

echo 'Running...'

for i in $TESTS_DIR/in/*;
do
    ${EXECUTABLE} $i > /dev/null 2>&1
done