function Pair(x, y) {
    return [
        { "first" : x },
        { "second" : y }
    ];
}

function Relation() {
    return [
        { "data" : [] },
        { "Insert" : (function(this, x, y) {
            assert(typeof(this) == "object");

            this.data[object_size(this.data)] = Pair(x, y);
        }) },
        { "Print" : (function(this) {
            assert(typeof(this) == "object");

            for(i = 0; i < object_size(this.data); ++i)
                sprint("(", this.data[i].first, ", ", this.data[i].second ,")");

            this.data[object_size(this.data)] = Pair(x, y);
        }) },
        { "Contains" : (function(this, x, y) {
            assert(typeof(this) == "object");

            for(i = 0; i < object_size(this.data); ++i) {
                if (this.data[i].first == x and
                    this.data[i].second == y) return true;
            }

            return false;
        }) },
        { "Clone" : (function(this) {
            assert(typeof(this) == "object");

            clone = Relation();

            for(i = 0; i < object_size(this.data); ++i)
                clone..Insert(this.data[i].first, this.data[i].second);

            return clone;
        }) },
        { "IsEmpty" : (function(this) {
            assert(typeof(this) == "object");
            return object_size(this.data) == 0;
        }) },
        { "ToString" : (function(this) {
            assert(typeof(this) == "object");

            local str = "";

            for(i = 0; i < object_size(this.data); ++i) {
                str = str + "(" + this.data[i].first + ", " + this.data[i].second + ") ";
            }

            return str;
        }) }
    ];
}

function ReflexiveClosure(relation, domain) {
    local R = relation..Clone();

    for(i = 0; i < object_size(domain); ++i) {
        val = domain[i];
        if (not R..Contains(val, val)) R..Insert(val, val);
    }

    return R;
}

function SymmetricClosure(relation) {
    local R = relation..Clone();

    size = object_size(relation.data);

    for(i = 0; i < size; ++i) {
        pair = relation.data[i];

        if (not relation..Contains(pair.second, pair.first))
            R..Insert(pair.second, pair.first);
    }

    return R;
}

local exam = Relation();
exam..Insert("1", "2");
exam..Insert("3", "4");
exam..Insert("1", "3");
exam..Insert("2", "1");

local dom = [ "1", "2", "3", "4", "5" ];

local ref = ReflexiveClosure(exam, dom);
local rsc = ReflexiveClosure(SymmetricClosure(exam), dom);

print("Relation:");
print(exam..ToString());
print("Reflexive Closure:");
print(ref..ToString());
print("Reflexive-Symmetric Closure:");
print(rsc..ToString());
sprint("\n");

local Exercise = Relation();

Exercise..Insert("1", "1");
Exercise..Insert("2", "2");
Exercise..Insert("2", "1");
Exercise..Insert("3", "3");
Exercise..Insert("1", "2");

rc = ReflexiveClosure(Exercise, dom);
sc = SymmetricClosure(Exercise);

print("Relation:");
print(Exercise..ToString());
print("Reflexive Closure:");
print(rc..ToString());
print("Symmetric Closure:");
print(sc..ToString());