/* ****************** Stack ****************** */

function SetMethod(obj, name, func) {
    obj[name] = func;
    obj[name].$closure = [
        { "$local" : obj },
        { "$outer" : func.$closure }
    ];
}

function Stack() {
    function Push(value) {
        assert(typeof(data) == "object");
        assert(typeof(size) == "number");

        data[size++] = value;
    }

    function Pop() {
        assert(typeof(data) == "object");
        assert(typeof(size) == "number");

        data[--size] = nil;
    }

    function Top() {
        assert(typeof(data) == "object");
        assert(typeof(size) == "number");

        return data[size - 1];
    }

    function TopAndPop() {
        assert(typeof(data) == "object");
        assert(typeof(size) == "number");

        val = Top();
        Pop();
        return val;
    }

    function IsEmpty() {
        assert(typeof(data) == "object");
        assert(typeof(size) == "number");

        return (size == 0);
    }

    function GetSize() {
        assert(typeof(data) == "object");
        assert(typeof(size) == "number");

        return size;
    }

    function Print() {
        assert(typeof(data) == "object");
        assert(typeof(size) == "number");

        for(i = size - 1; i >= 0; --i) print(data[i]);
    }

    function Clear() {
        assert(typeof(data) == "object");
        assert(typeof(size) == "number");

        while(not IsEmpty()) Pop();
    }

    local object = [
        { "size" : 0 },
        { "data" : [ ] }
    ];

    SetMethod(object, "Push", Push);
    SetMethod(object, "Pop", Pop);
    SetMethod(object, "Top", Top);
    SetMethod(object, "TopAndPop", TopAndPop);
    SetMethod(object, "IsEmpty", IsEmpty);
    SetMethod(object, "GetSize", GetSize);
    SetMethod(object, "Print", Print);
    SetMethod(object, "Clear", Clear);

    return object;
}

/* ****************** Stack Testing ****************** */

print("Stack testing");

stack = Stack();
print(stack.IsEmpty());

stack.Push(10);
print(stack.IsEmpty());

stack.Push(100);
print(stack.GetSize());

print(stack.Top());

stack.Pop();
stack.Push(20);
stack.Push(30);
stack.Push(40);
stack.Push(50);

stack.Print();

stack.Clear();
print(stack.GetSize());
print(stack.IsEmpty());