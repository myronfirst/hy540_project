/*
    Test file for the final phase of HY-340: Languages & Compilers
    Computer science dpt, University of Crete, Greece

    Expected Output:
    [ { a : 1.000 }, { b : 2.000 }, { c : 3.000 }, { e : 5.000 }, { f : 6.000 } ]
    5.000
    a b c e f
    Must print 0: 0.000
    Must print true (shallow copy): true
    WARNING: table[b] not found! at line 28
    Must print true (shallow copy): true
    Must print 'Hello, World!' (shallow copy):
    Must print: variables: 4, functions: 2, tables: 1
    variables:4.000 , functions:2.000 , tables:1.000
*/

nl = "\n";
t = [ {"a" : 1}, {"b" : 2}, {"c" : 3}, {"e" : 5}, {"f" : 6} ];

print(t,nl);
print(object_size(t), nl);

keys = object_keys(t);
n_elems = object_size(t);

for(i = 0; i < n_elems; ++i)
{
    print(keys[i]);
    t[keys[i]] = nil;
}

print("Must print 0: ", object_size(t), nl);

///////////////////////////////////////////////////////////////////////////////

t1 = [ {"t" : [ {"a" : 1}, {"b" : 2}, {"c" : 3}, {"d" : 4}, {"e" : 5}] } ];
t2 = t1;

t1["t"]["a"] = 13;
print("Must print true (shallow copy): ", t1["t"]["a"] == t2["t"]["a"], nl);

t1["t"]["b"] = nil;
print("Must print true (shallow copy): ", object_size(t1) == object_size(t2), nl);

print("Must print 'Hello, World!' (shallow copy): ", nl);
t1["t"]["()"] = (function () { print("Hello, World!", nl); } );
t2["t"](); // Functor call equivalent to t2["t"]["()"]();

/////////////////////////////////////////////////////////////////////////////

function count_table_type(table, type)
{
    if(typeof(table) != "object" or typeof(type) != "string")
    {
        return;
    }
    
    keys = object_keys(t);
    n_elems = object_size(t);
    n = 0;  
        
    for(i = 0; i < n_elems; ++i)
    {
        if(typeof(t[keys[i]]) == type)
        {
            ++n;
        }
    }
    
    return n;
}

t = [ 
        {"info" : (function(self) 
                    { 
                        if(typeof(self) == "object") 
                        { 
                            n_vars = count_table_type(self, "number") +
                            count_table_type(self, "boolean") + count_table_type(self, "string");
                            n_funcs = count_table_type(self, "program function");
                            n_tables = count_table_type(self, "object");
                                        
                            print("variables:", n_vars, ", functions:", n_funcs, ", tables:", n_tables, nl);
                        }
                    })
        },
        {"b" : 2}, {"c" : 3}, {"d" : true}, {"e" : "lala"}, {"f" : (function () {})}, {"g" : [1, 2, 3]}
    ];
        
print("Must print: variables: 4, functions: 2, tables: 1", nl);
t..info();